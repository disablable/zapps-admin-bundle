<?php

namespace Zapps\AdminBundle\Helper;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Zapps\AdminBundle\Entity\PageTemplateField;

class PageTemplateFieldHelper
{
    public static function getTypeFormChoices()
    {
        return [
            'Text' => PageTemplateField::TYPE_TEXT,
            'Textarea' => PageTemplateField::TYPE_TEXTAREA,
            'CKEditor' => PageTemplateField::TYPE_CKEDITOR,
            'Entity' => PageTemplateField::TYPE_ENTITY,
            'JSON' => PageTemplateField::TYPE_JSON,
        ];
    }

    public static function getSymfonyFormType($type)
    {
        $symfonyFormTypes = [
            PageTemplateField::TYPE_TEXT => TextType::class,
            PageTemplateField::TYPE_TEXTAREA => TextareaType::class,
            PageTemplateField::TYPE_CKEDITOR => CKEditorType::class,
            PageTemplateField::TYPE_ENTITY => EntityType::class,
            PageTemplateField::TYPE_JSON => TextareaType::class,
        ];

        if (isset($symfonyFormTypes[$type])) {
            return $symfonyFormTypes[$type];
        }
        // BC for old symfony types saved in DB. Remove later!!
        else {
            return $type;
        }
    }
}
