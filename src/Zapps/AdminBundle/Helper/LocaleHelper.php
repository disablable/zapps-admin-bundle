<?php

namespace Zapps\AdminBundle\Helper;

class LocaleHelper
{
    const LOCALE_EN = 'en';
    const LOCALE_DE = 'de';
    const LOCALE_HR = 'hr';

    public static function getLocaleFormChoices()
    {
        return [
            'English' => self::LOCALE_EN,
            'Deutsch' => self::LOCALE_DE,
            'Hrvatski' => self::LOCALE_HR,
        ];
    }
}
