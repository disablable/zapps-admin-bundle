<?php

namespace Zapps\AdminBundle\Helper;

/**
 * UserHelper
 */
class UserHelper
{
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';    // Can add/edit/delete super admins and admins.
    const ROLE_ADMIN = 'ROLE_ADMIN';                // Can login in administration, add/edit/delete user with lower roles.
    const ROLE_GEO_ADMIN = 'ROLE_GEO_ADMIN';
    const ROLE_NEWS_ADMIN = 'ROLE_NEWS_ADMIN';

    static public function getUserRoles()
    {
        return array(
            self::ROLE_SUPER_ADMIN => 'Super admin',
            self::ROLE_ADMIN => 'Admin',
            self::ROLE_GEO_ADMIN => 'Geo admin',
            self::ROLE_NEWS_ADMIN => 'News admin',
        );
    }
}