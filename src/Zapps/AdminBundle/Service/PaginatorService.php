<?php

namespace Zapps\AdminBundle\Service;

class PaginatorService
{
    public function getOffset($resultsPerPage, $page)
    {
        $offset = ( $resultsPerPage * $page ) - $resultsPerPage;

        return $offset < 0 ? 0 : $offset;
    }

    public function getTotalPages($totalResults, $resultsPerPage)
    {
        return ceil( $totalResults / $resultsPerPage );
    }

    public function getFirstResult($resultsPerPage, $page)
    {
        return ( $resultsPerPage * $page ) - $resultsPerPage + 1;
    }

    public function getLastResult($resultsPerPage, $page)
    {
        return ( $resultsPerPage * $page );
    }

    public function getPreviousPage($page)
    {
        if(($page - 1) > 0)
            return ($page - 1);

        return null;
    }

    public function getNextPage($page, $totalPages)
    {
        if(($page + 1) <= $totalPages)
            return ($page + 1);

        return null;
    }
}