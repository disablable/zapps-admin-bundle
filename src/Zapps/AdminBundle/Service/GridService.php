<?php

namespace Zapps\AdminBundle\Service;

use Symfony\Component\Form\FormFactory;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Zapps\AdminBundle\Service\PaginatorService;
use Zapps\AdminBundle\Grid\Common\AbstractColumn;
use Zapps\AdminBundle\Grid\Common\AbstractGrid;
use Zapps\AdminBundle\Grid\Common\AbstractFilter;
use Zapps\AdminBundle\Grid\Common\GridEntity;
use Zapps\AdminBundle\Grid\Row\HeaderRow;
use Zapps\AdminBundle\Grid\Row\BodyRow;

class GridService
{
    private $formFactory;
    private $em;
    private $paginatorService;

    public function __construct(FormFactory $formFactory, EntityManager $em, PaginatorService $paginatorService)
    {
        $this->formFactory = $formFactory;
        $this->em = $em;
        $this->paginatorService = $paginatorService;
    }

    public function createGrid(AbstractGrid $grid)
    {
        $gridEntity = new GridEntity();
        $grid->setEntity($gridEntity);

        $grid->buildGrid($this->em);

        $gridForm = $this->buildForm($grid);
        $grid->setForm($gridForm);

        return $grid;
    }

    public function bindRequest(AbstractGrid $grid, Request $request)
    {
        $form = $grid->getForm();
        $form->bind($request);

        // We intentionally "fix" any invalid page values
        $page = $grid->getGridEntity()->getPage();
        $options = [
            'options' => [
                'default' => 1, // value to return if the filter fails
                'min_range' => 1
        ]];
        $grid->getGridEntity()->setPage(filter_var($page, FILTER_VALIDATE_INT, $options));

        if(!$form->isValid())
            throw new \Exception("Grid form contains invalid data");
    }

    public function createView(AbstractGrid $grid)
    {
        $grid->setRows($this->loadGridData($grid));

        $routes = $grid->getRoutes();

        return array(
            'block_prefix' => $grid->getBlockPrefix(),
            'new_route' => $routes['new'],
            'form' => $grid->getForm()->createView(),
            'columns' => $this->createColumnsView($grid),
            'rows' => $this->createRowsView($grid),
            'paginator' => $this->createPaginatorView($grid),
            'filter_is_active' => $this->isFilterActive($grid),
        );
    }

    private function buildForm(AbstractGrid $grid)
    {
        $formBuilder = $this->formFactory->createNamedBuilder(
            $grid->getBlockPrefix(),
            'Symfony\Component\Form\Extension\Core\Type\FormType',
            $grid->getGridEntity(),
            ['csrf_protection' => false]
        );

        // filters form
        $filtersForm = $formBuilder->create('filters', 'form', array('compound' => true));
        foreach ($grid->getFilters() as $filter) {
            $filtersForm->add($filter->getName(), $this->resolveFilterFormType($filter), $filter->getOptions());
        }

        // main grid form
        $form = $formBuilder
            //change to HiddenType
            ->add('page', HiddenType::class, [
                'required' => false,
            ])
            ->add('sort_column', HiddenType::class, [
                'required' => false,
            ])
            ->add('sort_direction', HiddenType::class, [
                'required' => false,
            ])
            ->add('selected', HiddenType::class, [
                'required' => false,
            ])
            ->add($filtersForm, [
                'label' => false,
            ])
            ->getForm()
        ;

        return $form;
    }

    private function resolveFilterFormType(AbstractFilter $filter)
    {
        $formType = false;

        switch($filter->getType())
        {
            case 'text':
                $formType = 'text';
                break;

            case 'select':
                $formType = 'choice';
                break;

            case 'entity':
                $formType = 'entity';
                break;

            default:
                throw new \Exception(sprintf('Grid filter type "%s" is not defined', $filter->getType()));
                break;
        }

        return $formType;
    }

    private function loadGridData(AbstractGrid $grid)
    {
        // columns
        $columns = array();
        foreach ($grid->getColumns() as $column) {
            $columns[] = $column->getName();
        }

        // filters
        $filters = array();
        $filterValues = $grid->getGridEntity()->getFilters();
        foreach ($grid->getFilters() as $filter) {
            $filters[] = $filter->createRepositoryData($filterValues[$filter->getName()]);
        }

        // sort
        $sort = array(
            'column' => $grid->getGridEntity()->getSortColumn(),
            'direction' => $grid->getGridEntity()->getSortDirection(),
        );

        // limit
        $limit = array(
            'offset' => $this->paginatorService->getOffset($grid->getOption('results_per_page'), $grid->getGridEntity()->getPage()),
            'max_results' => $grid->getOption('results_per_page'),
        );

        // Get data for grid
        $data = $grid->getGridData($this->em, $grid->getEntityName(), $columns, $filters, $sort, $limit);

        return $data;
    }

    private function createColumnsView(AbstractGrid $grid)
    {
        foreach ($grid->getColumns() as $column) {
            // Define sort direction for sorted column
            $sortDirection = null;
            if ($grid->getGridEntity()->getSortColumn() == $column->getName()
                && $grid->getGridEntity()->getSortDirection() == 'asc'
            ) {
                $sortDirection = AbstractColumn::SORT_DIRECTION_UP;
            } elseif ($grid->getGridEntity()->getSortColumn() == $column->getName()
                && $grid->getGridEntity()->getSortDirection() == 'desc'
            ) {
                $sortDirection = AbstractColumn::SORT_DIRECTION_DOWN;
            }
            $column->setSortDirection($sortDirection);
        }

        return $grid->getColumns();
    }

    private function createRowsView(AbstractGrid $grid)
    {
        $selected = $grid->getGridEntity()->getSelected();

        $rows = array();

        foreach($grid->getRows() as $rowData)
        {
            $routes = $grid->getRoutes();
            $options = array(
                'selected' => false,    //array_key_exists($rowData->getId(), $selected) ? true : false,  // Use this if you want to recheck checked checkboxes after grid reload.
                'edit_route' => $routes['edit'],
                'delete_route' => $routes['delete'],
            );
            $rows[] = new BodyRow($rowData, $options);
        }

        return $rows;
    }

    private function createPaginatorView(AbstractGrid $grid)
    {
        return array(
            'page' => $grid->getGridEntity()->getPage(),
            'total_pages' => $this->paginatorService->getTotalPages(count($grid->getRows()), $grid->getOption('results_per_page')),
            'total_results' => count($grid->getRows()),
            'first_result' => $this->paginatorService->getFirstResult($grid->getOption('results_per_page'), $grid->getGridEntity()->getPage()),
            'last_result' => $this->paginatorService->getLastResult($grid->getOption('results_per_page'), $grid->getGridEntity()->getPage()),
            'previous_page' => $this->paginatorService->getPreviousPage($grid->getGridEntity()->getPage()),
            'next_page' => $this->paginatorService->getNextPage($grid->getGridEntity()->getPage(), $this->paginatorService->getTotalPages(count($grid->getRows()), $grid->getOption('results_per_page'))),
        );
    }

    private function isFilterActive(AbstractGrid $grid)
    {
        // If any filter field in form has data, return true
        if (!empty($grid->getForm()->getData()->getFilters())) {
            foreach ($grid->getForm()->getData()->getFilters() as $filterData) {
                if ($filterData) {
                    return true;
                }
            }
        }

        return false;
    }
}
