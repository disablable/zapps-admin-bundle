<?php

namespace Zapps\AdminBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class JsonConstraint extends Constraint
{
    public $message = 'This value should be valid JSON object.';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
