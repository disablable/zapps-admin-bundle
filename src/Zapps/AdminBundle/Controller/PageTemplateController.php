<?php

namespace Zapps\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Zapps\AdminBundle\Controller\CrudController;
use Zapps\AdminBundle\Grid\PageTemplateGrid;
use Zapps\AdminBundle\Entity\PageTemplate;
use Zapps\AdminBundle\Form\PageTemplateType;

/**
 * @Route("/page-template")
 * @Template()
 */
class PageTemplateController extends CrudController
{
    /**
     * @Route("/list", name="admin_page_template_list")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $gridClass = new PageTemplateGrid(['results_per_page' => 100]);

        return $this->crudListAction($request, $gridClass);
    }

    /**
     * @Route("/new", name="admin_page_template_new")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $pageTemplate = new PageTemplate();
        $pageTemplateType = new PageTemplateType();

        return $this->crudNewAction($request, $pageTemplate, $pageTemplateType);
    }

    /**
     * @Route("/{id}/edit", name="admin_page_template_edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $pageTemplate = new PageTemplate();
        $pageTemplateType = new PageTemplateType();

        return  $this->crudEditAction($request, $id, $pageTemplate, $pageTemplateType);
    }

    /**
     * @Route("/{id}/delete", name="admin_page_template_delete")
     * @Template()
     */
    public function deleteAction(Request $request, $id)
    {
        $pageTemplate = new PageTemplate();

        return $this->crudDeleteAction($request, $id, $pageTemplate);
    }
}
