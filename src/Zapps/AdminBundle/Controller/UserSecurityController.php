<?php

namespace Zapps\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

use FOS\UserBundle\Controller\SecurityController as FOSSecurityController;

/**
 * User security controller.
 *
 * This controller is taken from FOSUserBundle:Security controller
 */
class UserSecurityController extends FOSSecurityController
{
    /**
     * User login
     *
     * @Route("/login", name="admin_user_login")
     * @Template()
     */
    public function loginAction()
    {
        return parent::loginAction();
    }

    /**
     * Renders the login template with the given parameters. Overwrite this function in
     * an extended controller to provide additional data for the login template.
     *
     * @param array $data
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderLogin(array $data)
    {
        $template = sprintf('ZappsAdminBundle:UserSecurity:login.html.%s', $this->container->getParameter('fos_user.template.engine'));

        return $this->container->get('templating')->renderResponse($template, $data);
    }

    /**
     * User check
     *
     * @Route("/check", name="admin_user_login_check")
     * @Template()
     */
    public function checkAction()
    {
        parent::checkAction();
    }

    /**
     * User logout
     *
     * @Route("/logout", name="admin_user_logout")
     * @Template()
     */
    public function logoutAction()
    {
        parent::logoutAction();
    }
}
