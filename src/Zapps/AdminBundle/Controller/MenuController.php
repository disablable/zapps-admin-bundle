<?php

namespace Zapps\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class MenuController extends Controller
{
    /**
     * @Template()
     */
    public function indexAction($activePath)
    {
        $menu = $this->container->getParameter('zapps_admin.menu');

        // Detect active route (without route locale)
        // $activePath = array_filter(explode('/', $activePath));
        // foreach ($activePath as $key => $val) {
        //     if ($val == 'admin') {
        //         unset($activePath[$key]);
        //     }
        //     break;
        // }
        // $activePath = implode('/', $activePath);


        // Detect active route (with route locale)
        // Extract the part of activePath that is comperable with congif menu paths
        // E.g. "/admin/en/user/list" will be reduced to "user/list"
        $activePath = preg_replace('/\/admin\/[A-z].\//', '', $activePath);

        return [
            'menu' => $menu,
            'active_path' => empty($activePath) ? '/' : $activePath,
        ];
    }
}
