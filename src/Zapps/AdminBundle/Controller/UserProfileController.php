<?php

namespace Zapps\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Zapps\AdminBundle\Controller\CrudController;
use Zapps\AdminBundle\Entity\User;
use Zapps\AdminBundle\Grid\UserProfileGrid;
use Zapps\AdminBundle\Form\UserCreateProfileType;
use Zapps\AdminBundle\Form\UserEditProfileType;
use Zapps\AdminBundle\Form\UserEditRolesType;
use Zapps\AdminBundle\Helper\UserHelper;

/**
 * Controller managing the user profile
 *
 * @Route("/user-profile")
 */
class UserProfileController extends CrudController
{
    /**
     * Lists all entities.
     *
     * @Route("/list", name="admin_user_profile_list")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function listAction(Request $request)
    {
        $gridClass = new UserProfileGrid([
            'results_per_page' => 100,
            'auth_user_roles' => $this->getUser()->getRoles(),
        ]);

        return $this->crudListAction($request, $gridClass);
    }

    /**
     * Displays a form to create a new entity
     * and persists submited new entity.
     *
     * @Route("/new", name="admin_user_profile_new")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();

        $form = $this->createForm(new UserCreateProfileType(), $user);

        // if form is submited
        if($request->getMethod() === 'POST')
        {
            // bind form
            $form->bind($request);

            // if form is valid
            if($form->isValid())
            {
                $userManager->updateUser($user);

                // close window
                return $this->render('ZappsAdminBundle:Window:close.html.twig');
            }
        }

        return array(
            'entity' => $user,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing entity by id
     * and persists changes on submited entity.
     *
     * @Route("/{id}/edit", name="admin_user_profile_edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));
        if(!$user) {
            throw $this->createNotFoundException('Unable to find user');
        }

        // If authenticated user is not SUPER_ADMIN, don't let him edit users with roles ROLE_SUPER_ADMIN and ROLE_ADMIN
        // If user is admin he can edit his own data (OVO SE MIJENJA, SVAKI USER MOŽE EDITIRATI SVOJ PROFIL!!)
        if($this->get('security.context')->isGranted(UserHelper::ROLE_SUPER_ADMIN) === false
            && ($user->hasRole(UserHelper::ROLE_SUPER_ADMIN) || $user->hasRole(UserHelper::ROLE_ADMIN))
            && $this->get('security.context')->getToken()->getUser()->getId() != $user->getId()) {
            throw new AccessDeniedException();
        }

        $form = $this->createForm(new UserEditProfileType(), $user, array('is_super_admin' => $this->get('security.context')->isGranted(UserHelper::ROLE_SUPER_ADMIN)));

        // if form is submited
        if($request->getMethod() === 'POST')
        {
            // bind form
            $form->bind($request);

            // if form is valid
            if($form->isValid())
            {
                $userManager->updateUser($user);


                // TODO: Ako je editiran user zapravo onaj koji je logiran, potrebno
                // je relogirati tog usera, da se osvježe njegove dozvole (ROLES) itd...
                // Do ove situacije će doći samo ako je user SUPER_ADMIN jer u tom slučaju on
                // može editirati i sam sebe. Ovo ne znam točno kako napraviti...


                // close window
                return $this->render('ZappsAdminBundle:Window:close.html.twig');
            }
        }

        return array(
            'entity' => $user,
            'form' => $form->createView(),
        );
    }

    /**
     * Deletes entity by id.
     *
     * @Route("/{id}/delete", name="admin_user_profile_delete")
     * @Security("has_role('ROLE_ADMIN')")
     * @Template()
     */
    public function deleteAction(Request $request, $id)
    {
        // If authenticated user is not SUPER_ADMIN but is ADMIN, he can not delete other users with
        // ROLE_SUPER_ADMIN and ROLE_ADMIN
        if($this->get('security.context')->isGranted(UserHelper::ROLE_SUPER_ADMIN) === false)
        {
            if($user->hasRole(UserHelper::ROLE_SUPER_ADMIN) || $user->hasRole(UserHelper::ROLE_ADMIN)) {
                throw new AccessDeniedException();
            }
        }

        $user = new User();

        return $this->crudDeleteAction($request, $id, $user);
    }

    /**
     * Edit user roles
     *
     * @Route("/roles/{id}/edit", name="admin_user_roles_edit")
     * @Template()
     */
    public function editRolesAction(Request $request, $id)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));
        if(!$user) {
            throw $this->createNotFoundException('Unable to find user');
        }

        // If authenticated user is not SUPER_ADMIN or ADMIN he can not manage his own roles or roles of other users
        if($this->get('security.context')->isGranted(UserHelper::ROLE_ADMIN) === false) {
            throw new AccessDeniedException();
        }

        $userRoles = $user->getRoles();
        $form = $this->createForm(new UserEditRolesType(), array('roles' => $userRoles));

        // if form is submited
        if($request->getMethod() === 'POST')
        {
            // bind form
            $form->bind($request);

            $submitedUserRoles = $form->getData();
            $submitedUserRoles = $submitedUserRoles['roles'];

            // if form is valid
            if($form->isValid())
            {
                // Apply user roles to user
                $roles = array();

                // If authenticated user is not SUPER_ADMIN he didn't have roles SUPER_ADMIN and ADMIN listed in
                // roles choice list when he was editing current user.
                // In case that edited user had roles SUPER_ADMIN or ADMIN we must make sure that those roles
                // are not overwritten by submited form data!!
                if($this->get('security.context')->isGranted(UserHelper::ROLE_SUPER_ADMIN) === false)
                {
                    if(in_array(UserHelper::ROLE_SUPER_ADMIN, $userRoles)) {
                        $roles[] = UserHelper::ROLE_SUPER_ADMIN;
                    }
                    if(in_array(UserHelper::ROLE_ADMIN, $userRoles)) {
                        $roles[] = UserHelper::ROLE_ADMIN;
                    }
                }

                $roles = array_merge($roles, $submitedUserRoles);
                $user->setRoles($roles);

                $userManager->updateUser($user);

                // close window
                return $this->render('ZappsAdminBundle:Window:close.html.twig');
            }
        }

        return array(
            'entity' => $user,
            'form' => $form->createView(),
        );
    }

    /**
     * List user roles
     *
     * @Route("/roles/{id}/list", name="admin_user_roles_list")
     * @Template()
     */
    public function listRolesAction(Request $request, $id)
    {
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));
        if(!$user) {
            throw $this->createNotFoundException('Unable to find user');
        }

        $userRoles = array();
        foreach(UserHelper::getUserRoles() as $key => $role)
        {
            if(in_array($key, $user->getRoles())) {
                $userRoles[] = $role;
            }
        }

        return array('user_roles' => $userRoles);
    }
}
