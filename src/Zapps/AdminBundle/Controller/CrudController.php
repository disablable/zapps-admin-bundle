<?php

namespace Zapps\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Crud controller
 */
class CrudController extends Controller
{
    /**
     * Crud "list" action
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string $entityName (e.g. 'Category')
     * @return array Array with data for Crud:list.html.twig template
     */
    protected function crudListAction(Request $request, $gridClass)
    {
        $gridService = $this->get('zapps_admin.grid_service');
        $grid = $gridService->createGrid($gridClass);

        if($request->getMethod() == 'POST')
            $gridService->bindRequest($grid, $request);

        return array(
            'grid' => $gridService->createView($grid),
        );
    }

    /**
     * Crud "new" action
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param string $entityName (e.g. 'Category')
     * @return array Array with data for Crud:new.html.twig template
     */
    protected function crudNewAction(Request $request, $entity, $formType)
    {
        $form = $this->createForm($formType, $entity);

        // if form is submited
        if($request->getMethod() === 'POST')
        {
            // bind form
            $form->handleRequest($request);

            // if form is valid
            if($form->isValid())
            {
                // persist entity
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                // close window
                return $this->render('ZappsAdminBundle:Window:crudClose.html.twig', ['new_id' => $entity->getId()]);
            }
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Crud "edit" action
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param integer $id Id of entity to be edited
     * @param string $entityName (e.g. 'Category')
     * @return array Array with data for Crud:edit.html.twig template
     */
    protected function crudEditAction(Request $request, $id, $entity, $formType)
    {
        $em = $this->getDoctrine()->getManager();

        $entityClassName = get_class($entity);
        $entity = $em->getRepository($entityClassName)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException(sprintf('Unable to find entity %s', $entityClassName));
        }

        $form = $this->createForm($formType, $entity);

        // if form is submited
        if($request->getMethod() === 'POST') {

            // If this is translatable entity
            if (method_exists($entity, 'getTranslationEntityClass')) {
                // This hack allows to remove existing translaton from entity if user deletes data
                // from form's translatable fields for certain locale.

                // Get form data directly from request
                $formData = $request->request->get($form->getName());
                if (isset($formData['translations']) && is_array($formData['translations'])) {
                    // Iterate through each locale
                    foreach ($formData['translations'] as $locale => $translation) {
                        // Iterate through each field for current locale and checkif
                        // all of it's data is empty
                        $localeDataIsEmpty = true;
                        foreach ($translation as $data) {
                            if (!empty($data)) {
                                $localeDataIsEmpty = false;
                                break;
                            }
                        }
                        // If data for some locale is empty, remove translation for this locale from the entity
                        if ($localeDataIsEmpty) {
                            foreach ($entity->getTranslations() as $translation) {
                                if ($translation->getLocale() == $locale) {
                                    $entity->removeTranslation($translation);
                                }
                            }
                        }

                    }
                }

                // In the end, create new form with entity which have removed empty translation.
                // In that way the validation will not be triggered for this locale since it's empty.
                $form = $this->createForm($formType, $entity);
            }

            // bind request to form
            $form->handleRequest($request);

            // if form is valid
            if ($form->isValid())
            {
                // persist entitiy
                $em->persist($entity);
                $em->flush();

                // close window
                return $this->render('ZappsAdminBundle:Window:close.html.twig');
            }
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Crud "delete" action
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param integer $id Id of entity to be deleted
     * @param string $entityName (e.g. 'Category')
     * @return array Array with data for Crud:delete.html.twig template
     */
    protected function crudDeleteAction(Request $request, $id, $entity)
    {
        $form = $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm();

        if($request->getMethod() === 'POST')
        {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $entityClassName = get_class($entity);
                $entity = $em->getRepository($entityClassName)->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException(sprintf('Unable to find entity %s', $entityClassName));
                }

                $em->remove($entity);
                $em->flush();

                // close window
                return $this->render('ZappsAdminBundle:Window:close.html.twig');
            }
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }
}
