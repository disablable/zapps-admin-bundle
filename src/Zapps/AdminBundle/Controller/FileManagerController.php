<?php

namespace Zapps\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * File manager controller
 */
class FileManagerController extends Controller
{
    /**
     * Index
     *
     * @Route("photo-manager/", name="zapps_photo_manager")
     * @Template()
     */
    public function photoManagerAction()
    {
        

        return array();
    }
}
