<?php

namespace Zapps\AdminBundle\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Intl\DateFormatter\IntlDateFormatter;

class IntlExtension extends \Twig_Extension
{
    private $locale;

    public function __construct()
    {
        if (!class_exists('IntlDateFormatter')) {
            throw new \RuntimeException('The intl extension is needed to use intl-based filters.');
        }
    }

    public function setRequest(RequestStack $requestStack)
    {
        $request = $requestStack->getMasterRequest();
        if ($request) {
            $locale = $request->getLocale();
            if ($locale) {
                $this->locale = $locale;
            } else {
                $this->locale = $request->getDefaultLocale();
            }
        }
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('localizedmomentjsdatepattern', [$this, 'twig_localized_momentjs_date_pattern'], ['needs_environment' => true]),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('localizeddate', [$this, 'twig_localized_date_filter'], ['needs_environment' => true]),
        );
    }

    public function twig_localized_momentjs_date_pattern(\Twig_Environment $env, $dateFormat = 'medium', $timeFormat = 'medium', $locale = null, $timezone = null, $format = null)
    {
        $formatValues = array(
            'none' => \IntlDateFormatter::NONE,
            'short' => \IntlDateFormatter::SHORT,
            'medium' => \IntlDateFormatter::MEDIUM,
            'long' => \IntlDateFormatter::LONG,
            'full' => \IntlDateFormatter::FULL,
        );

        $momentPattern = $this->convertPHPToMomentJsPattern($formatValues[$dateFormat], $formatValues[$timeFormat], $locale);

        return $momentPattern;
    }

    public function twig_localized_date_filter(\Twig_Environment $env, $date, $dateFormat = 'medium', $timeFormat = 'medium', $locale = null, $timezone = null, $format = null)
    {
        if ($locale === null) {
            $locale = $this->locale;
        }

        if ($locale == 'en') {
            $locale = 'en_GB';
        }

        $date = twig_date_converter($env, $date, $timezone);

        $formatValues = array(
            'none' => \IntlDateFormatter::NONE,
            'short' => \IntlDateFormatter::SHORT,
            'medium' => \IntlDateFormatter::MEDIUM,
            'long' => \IntlDateFormatter::LONG,
            'full' => \IntlDateFormatter::FULL,
        );

        $formatter = \IntlDateFormatter::create(
            $locale,
            $formatValues[$dateFormat],
            $formatValues[$timeFormat],
            $date->getTimezone()->getName(),
            \IntlDateFormatter::GREGORIAN,
            $format
        );

        return $formatter->format($date->getTimestamp());
    }

    private function convertPHPToMomentJsPattern($datePattern, $timePattern, $locale = null)
    {
        if ($locale === null) {
            $locale = $this->locale;
        }

        if (!in_array($locale, ['en', 'de', 'hr'])) {
            throw new \Exception('Only the locales "en, de and hr" are supported in converting to momentjs library. Locale "'.$locale.'" provided. Please add more locale definitions to '.__CLASS__);
        }

        $pattern = [];
        $pattern[] = self::convertPHPToMomentJsDatePattern($datePattern, $locale);
        $pattern[] = self::convertPHPToMomentJsTimePattern($timePattern, $locale);
        $separator = self::getDateTimeSeparator($datePattern, $locale);

        return implode($separator, array_filter($pattern));
    }

    private static function convertPHPToMomentJsDatePattern($datePattern, $locale)
    {
        $momentPatterns = [
            'en' => [
                \IntlDateFormatter::NONE =>     null,
                \IntlDateFormatter::SHORT =>    'M/D/YY',
                \IntlDateFormatter::MEDIUM =>   'MMM D, YYYY',
                \IntlDateFormatter::LONG =>     'MMMM D, YYYY',
                \IntlDateFormatter::FULL =>     'dddd, MMMM D, YYYY',
            ],
            'de' => [
                \IntlDateFormatter::NONE =>     null,
                \IntlDateFormatter::SHORT =>    'DD.MM.YY',
                \IntlDateFormatter::MEDIUM =>   'DD.MM.YYYY',
                \IntlDateFormatter::LONG =>     'D. MMMM YYYY',
                \IntlDateFormatter::FULL =>     'dddd, D. MMMM YYYY',
            ],
            'hr' => [
                \IntlDateFormatter::NONE =>     null,
                \IntlDateFormatter::SHORT =>    'D.M.YY.',
                \IntlDateFormatter::MEDIUM =>   'D. MMM YYYY.',
                \IntlDateFormatter::LONG =>     'D. MMMM YYYY.',
                \IntlDateFormatter::FULL =>     'dddd, D. MMMM YYYY.',
            ],
        ];

        return $momentPatterns[$locale][$datePattern];
    }

    private static function convertPHPToMomentJsTimePattern($timePattern, $locale)
    {
        $momentPatterns = [
            'en' => [
                \IntlDateFormatter::NONE =>     null,
                \IntlDateFormatter::SHORT =>    'LT',
                \IntlDateFormatter::MEDIUM =>   'LTS',
                \IntlDateFormatter::LONG =>     'LTS',
                \IntlDateFormatter::FULL =>     'LTS',
            ],
            'de' => [
                \IntlDateFormatter::NONE =>     null,
                \IntlDateFormatter::SHORT =>    'HH:mm',
                \IntlDateFormatter::MEDIUM =>   'HH:mm:ss',
                \IntlDateFormatter::LONG =>     'HH:mm:ss',
                \IntlDateFormatter::FULL =>     'HH:mm:ss',
            ],
            'hr' => [
                \IntlDateFormatter::NONE =>     null,
                \IntlDateFormatter::SHORT =>    'HH:mm',
                \IntlDateFormatter::MEDIUM =>   'HH:mm:ss',
                \IntlDateFormatter::LONG =>     'HH:mm:ss',
                \IntlDateFormatter::FULL =>     'HH:mm:ss',
            ],
        ];

        return $momentPatterns[$locale][$timePattern];
    }

    private static function getDateTimeSeparator($datePattern, $locale)
    {
        $dateTimeSeparators = [
            'en' => [
                \IntlDateFormatter::NONE =>     '',
                \IntlDateFormatter::SHORT =>    ', ',
                \IntlDateFormatter::MEDIUM =>   ', ',
                \IntlDateFormatter::LONG =>     ' at ',
                \IntlDateFormatter::FULL =>     ' at ',
            ],
            'de' => [
                \IntlDateFormatter::NONE =>     '',
                \IntlDateFormatter::SHORT =>    ' ',
                \IntlDateFormatter::MEDIUM =>   ' ',
                \IntlDateFormatter::LONG =>     ' ',
                \IntlDateFormatter::FULL =>     ' ',
            ],
            'hr' => [
                \IntlDateFormatter::NONE =>     '',
                \IntlDateFormatter::SHORT =>    ' ',
                \IntlDateFormatter::MEDIUM =>   ' ',
                \IntlDateFormatter::LONG =>     ' u ',
                \IntlDateFormatter::FULL =>     ' u ',
            ],
        ];

        return $dateTimeSeparators[$locale][$datePattern];
    }

    public function getName()
    {
        return 'zapps_intl';
    }
}
