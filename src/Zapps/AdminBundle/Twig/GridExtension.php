<?php

namespace Zapps\AdminBundle\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Intl\DateFormatter\IntlDateFormatter;

class GridExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('grid', [$this, 'renderGrid', ['grid']], [
                'is_safe' => ['html'],
                'needs_environment' => true
            ]),
            new \Twig_SimpleFunction('grid_head', [$this, 'renderGridHead', ['grid']], [
                'is_safe' => ['html'],
                'needs_environment' => true
            ]),
            new \Twig_SimpleFunction('grid_body', [$this, 'renderGridBody', ['grid']], [
                'is_safe' => ['html'],
                'needs_environment' => true
            ]),
            new \Twig_SimpleFunction('grid_column', [$this, 'renderGridColumn', ['gridColumn']], [
                'is_safe' => ['html'],
                'needs_environment' => true
            ]),
            new \Twig_SimpleFunction('grid_paginator', [$this, 'renderGridPaginator', ['grid']], [
                'is_safe' => ['html'],
                'needs_environment' => true
            ]),
        ];
    }

    public function renderGrid(\Twig_Environment $twig, $grid)
    {
        return $twig->render('ZappsAdminBundle:Grid:block_grid.html.twig', ['grid' => $grid]);
    }

    public function renderGridHead(\Twig_Environment $twig, $grid)
    {
        return $twig->render('ZappsAdminBundle:Grid:block_grid_head.html.twig', ['grid' => $grid]);
    }

    public function renderGridBody(\Twig_Environment $twig, $grid)
    {
        return $twig->render('ZappsAdminBundle:Grid:block_grid_body.html.twig', ['grid' => $grid]);
    }

    public function renderGridColumn(\Twig_Environment $twig, $gridColumn, $rowValue)
    {
        $columnType = $gridColumn->getType();   // this must be the same as column widget block that will be rendered
        $template = $twig->loadTemplate('ZappsAdminBundle:Grid:block_grid_columns.html.twig');

        return $template->renderBlock($columnType, ['column' => $gridColumn, 'value' => $rowValue]);
    }

    public function renderGridPaginator(\Twig_Environment $twig, $grid)
    {
        return $twig->render('ZappsAdminBundle:Grid:block_grid_paginator.html.twig', ['grid' => $grid]);
    }

    public function getName()
    {
        return 'zapps_grid';
    }
}
