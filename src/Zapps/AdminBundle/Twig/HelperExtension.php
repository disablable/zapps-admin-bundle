<?php

namespace Zapps\AdminBundle\Twig;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Intl\DateFormatter\IntlDateFormatter;

class HelperExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('zappsMapper', function ($value, $method) {
                $method = explode('::', $method);
                $class = $method[0];
                $property = $method[1];
                if (is_callable($method)) {
                    return $class::$property($value);
                }

                return null;
            }),
        );
    }

    public function getName()
    {
        return 'zapps_helper';
    }
}
