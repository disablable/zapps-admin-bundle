<?php

namespace Zapps\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;

use Zapps\AdminBundle\Helper\UserHelper;

class UserRolesType extends AbstractType
{
    private $authUserRoles;

    public function __construct(SecurityContext $securityContext)
    {
        $this->authUserRoles = $securityContext->getToken()->getRoles();
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $choices = UserHelper::getUserRoles();
        $authUserRoles = array();

        foreach($this->authUserRoles as $authRole) {
            $authUserRoles[] = $authRole->getRole();
        }

        // if authenticated user doesn't have ROLE_SUPER_ADMIN, remove ROLE_SUPER_ADMIN and ROLE_ADMIN from choice list
        if(!in_array(UserHelper::ROLE_SUPER_ADMIN, $authUserRoles)) {
            unset($choices[UserHelper::ROLE_SUPER_ADMIN]);
            unset($choices[UserHelper::ROLE_ADMIN]);
        }

        $resolver->setDefaults(array(
            'choices' => $choices,
        ));
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'user_roles';
    }
}
