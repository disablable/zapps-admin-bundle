<?php

namespace Zapps\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Zapps\AdminBundle\Form\PageTemplateFieldType;

class PageTemplateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class);

        $builder->add('controller', TextType::class, [
            'attr' => ['placeholder' => 'e.g. AppBundle:Page:Homepage'],
        ]);

        $builder->add('fields', CollectionType::class, [
            'entry_type' => new PageTemplateFieldType(),
            'allow_add' => true,
            'allow_delete' => true,
            'prototype' => true,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Zapps\AdminBundle\Entity\PageTemplate',
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'zapps_pagetemplate';
    }
}
