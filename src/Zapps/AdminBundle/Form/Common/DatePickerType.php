<?php

namespace Zapps\AdminBundle\Form\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\CallbackTransformer;

class DatePickerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addViewTransformer(new CallbackTransformer(
            function ($dateTimeObject) {
                if ($dateTimeObject) {
                    return $dateTimeObject->format('Y-m-d');
                } else {
                    return null;
                }
            },
            function ($submittedDateString) {
                return new \DateTime($submittedDateString);
            }
        ));
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function getBlockPrefix()
    {
        return 'zapps_date';
    }
}
