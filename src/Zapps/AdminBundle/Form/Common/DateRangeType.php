<?php

namespace Zapps\AdminBundle\Form\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class DateRangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateRange', TextType::class, [
                'label' => false,
                'mapped' => false,
                'attr' => ['placeholder' => 'widget.daterange.placeholder']
            ])
            ->add($options['date_start_property'], HiddenType::class, [
                'label' => false,
                'error_bubbling' => true
            ])
            ->add($options['date_end_property'], HiddenType::class, [
                'label' => false,
                'error_bubbling' => true
            ])
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                function (FormEvent $event) use ($options) {

                    // Before submitting form with DateRangeType we must convert submitted dates (as string)
                    // in to DateTime objects before binding them to entity.

                    $data = $event->getData();

                    if (!empty($data[$options['date_start_property']])) {
                        $data[$options['date_start_property']] = new \DateTime($data[$options['date_start_property']]);
                    }

                    if (!empty($data[$options['date_end_property']])) {
                        $data[$options['date_end_property']] = new \DateTime($data[$options['date_end_property']]);
                    }

                    $event->setData($data);
                }
            );
        ;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        // Pass additional variables to template
        $view->vars['date_start_property'] = $options['date_start_property'];
        $view->vars['date_end_property'] = $options['date_end_property'];
        $view->vars['invalid_dates'] = implode(',', $options['invalid_dates']);
        $view->vars['unclickable_dates'] = implode(',', $options['unclickable_dates']);
        $view->vars['use_half_dates'] = $options['use_half_dates'] === true ? 'true' : 'false';
        $view->vars['is_selected_range_valid'] = $options['is_selected_range_valid'];
        $view->vars['drops'] = $options['drops'];
        $view->vars['show_dates_from_other_months'] = $options['show_dates_from_other_months'] === true ? 'true' : 'false';
        $view->vars['min_date'] = $options['min_date'];
        $view->vars['max_date'] = $options['max_date'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,                 // This option lets the form inherit its data from its parent form. This enables subfields of this DateRangeType
                                                    // form ("date_start_property" and "date_end_property") to access form's entity properties.
            'cascade_validation' => true,

            // Specific DateRange options
            'date_start_property' => 'dateStart',   // This is property name from entity to which dateStart data will be binded
            'date_end_property' => 'dateEnd',       // This is property name from entity to which dateEnd data will be binded
            'invalid_dates' => [],                  // Array of dates that will be disabled for selection in "Y-m-d" format
            'unclickable_dates' => [],              // Array of dates that can not be clicked on, but can be inside of dateStart-dateEnd range. (in "Y-m-d" format)
            'use_half_dates' => false,              // This enables marking day before and day after invalid day(s) with triangles (suitable for rental check-in/check-out)
            'is_selected_range_valid' =>            // This option defines JS function which is called after selecting some date range and after clicking on "Apply" button. Returns true|false.
                'function(daterangepicker) {
                    return true;
                }',
            'drops' => 'down',
            'show_dates_from_other_months' => false,    // This option shows days from prev and next months in current calendar
            'min_date' => null,                         // The earliest date user can select
            'max_date' => null,                         // The latest date user can select
        ]);
    }

    public function getBlockPrefix()
    {
        return 'zapps_daterange';
    }
}
