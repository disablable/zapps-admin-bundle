<?php

namespace Zapps\AdminBundle\Form\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Zapps\AdminBundle\Helper\LocalesHelper;

class AbstractTranslationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('object', null, [
                'required' => false,
            ])
            ->add('locale', 'choice', [
                'choices' => LocalesHelper::getLocales()
            ])
            ->add('field')
            ->add('content', 'text')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        throw new \Exception('You must implement "setDefaultOptions()" method in your class '. get_class($this));
    }

    public function getName()
    {
        throw new \Exception('You must implement "getName()" method in your class '. get_class($this));
    }
}
