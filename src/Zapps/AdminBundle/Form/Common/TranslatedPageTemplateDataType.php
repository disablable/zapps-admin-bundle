<?php

namespace Zapps\AdminBundle\Form\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\NotBlank;

use Zapps\AdminBundle\Entity\FormTemplateData;
use Zapps\AdminBundle\Entity\PageTemplate;
use Zapps\AdminBundle\Entity\PageTemplateField;
use Zapps\AdminBundle\Helper\PageTemplateFieldHelper;
use Zapps\AdminBundle\Validator\JsonConstraint;

class TranslatedPageTemplateDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //
        // Load form fields stored in PageTemplate and build the form fields
        //
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();
            $data = $event->getData();
            $pageTemplateFields = $this->getPageTemplateFields($form);

            // Get fields from $pageTemplate and add them as form fields
            foreach ($pageTemplateFields as $pageTemplateField) {
                $fieldFormType = PageTemplateFieldHelper::getSymfonyFormType($pageTemplateField->getType());

                $fieldOptions = $this->getFieldOptions($pageTemplateField);
                $form->add($pageTemplateField->getName(), $fieldFormType, $fieldOptions);

                // Also, if there is EntityType field, then we must convert it's id (saved in DB) into entity object
                if (!empty($data[$pageTemplateField->getName()])) {
                    if ($pageTemplateField->getType() == PageTemplateField::TYPE_ENTITY) {
                        $em = $form->get($pageTemplateField->getName())->getConfig()->getOption('em');
                        $entityClass = $form->get($pageTemplateField->getName())->getConfig()->getOption('class');
                        $entityId = $data[$pageTemplateField->getName()];
                        $entity = $em->getRepository($entityClass)->findOneById($entityId);
                        $data[$pageTemplateField->getName()] = $entity;
                    }
                }
            }

            $event->setData($data);
        });

        //
        // On form submit, before form validation, check for "required" fields.
        // Required constraints must be only applied to locales which have some data entered.
        // Also apply validators for filled types that need validation.
        //
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();  // This is templateData form field
            $data = $event->getData();  // This is templateData form field's data
            $pageTemplateFields = $this->getPageTemplateFields($form);

            // Locale for which data is currently provided in listener.
            $locale = $form->getParent()->getName();

            // Check if all data for the current locale is empty.
            $localeDataIsEmpty = true;
            foreach ($pageTemplateFields as $pageTemplateField) {
                $fieldName = $pageTemplateField->getName();
                if (!empty($data[$fieldName])) {
                    $localeDataIsEmpty = false;
                    break;
                }
            }

            if (!$localeDataIsEmpty) {
                foreach ($pageTemplateFields as $pageTemplateField) {
                    $fieldFormType =  PageTemplateFieldHelper::getSymfonyFormType($pageTemplateField->getType());

                    // Apply field options

                    $fieldOptions = $this->getFieldOptions($pageTemplateField);
                    $fieldRequiredOptions = $this->getRequiredFieldOptions($pageTemplateField);
                    $fieldOptions = array_merge_recursive($fieldOptions, $fieldRequiredOptions);

                    $fieldValidatorOptions = $this->getFieldValidatorOptions($pageTemplateField);
                    $fieldOptions = array_merge_recursive($fieldOptions, $fieldValidatorOptions);

                    $form->add($pageTemplateField->getName(), $fieldFormType, $fieldOptions);
                }
            }
        });

        //
        // On form submit, if there are EntityType fields, we must convert entity class(es) to integers (as their ids)
        //
        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();  // This is templateData form field
            $data = $event->getData();  // This is templateData form field's data

            $pageTemplateFields = $this->getPageTemplateFields($form);
            foreach ($pageTemplateFields as $pageTemplateField) {
                if ($pageTemplateField->getType() == PageTemplateField::TYPE_ENTITY && !empty($data[$pageTemplateField->getName()])) {
                    $data[$pageTemplateField->getName()] = $data[$pageTemplateField->getName()]->getId();
                }
            }

            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'template' => null,
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'zapps_formtemplate';
    }

    // Get PageTemplate fields from form options
    private function getPageTemplateFields($form)
    {
        $pageTemplate = $form->getConfig()->getOption('template');

        if (empty($pageTemplate) || !($pageTemplate instanceof PageTemplate)) {
            throw new \LogicException(
                'The PageTemplateDataType option "template" must be instance of Zapps\AdminBundle\Entity\PageTemplate'
            );
        }

        // Get common fields for all page templates
        $pageTemplateFields = $this->getCommonPageTemplateFields();

        // Get user defined fields for specific page template
        foreach ($pageTemplate->getFields() as $pageTemplateField) {
            $pageTemplateFields[] = $pageTemplateField;
        }

        return $pageTemplateFields;
    }

    private function getCommonPageTemplateFields()
    {
        $commonFields = [];

        // slug
        $pageTemplateField = new PageTemplateField();
        $pageTemplateField->setName('slug');
        $pageTemplateField->setType(PageTemplateField::TYPE_TEXT);
        $pageTemplateField->setOptions('{"required": true}');
        $commonFields[] = $pageTemplateField;

        // pageTitle
        $pageTemplateField = new PageTemplateField();
        $pageTemplateField->setName('pageTitle');
        $pageTemplateField->setType(PageTemplateField::TYPE_TEXT);
        $pageTemplateField->setOptions('{"required": true}');
        $commonFields[] = $pageTemplateField;

        // metaDescription
        $pageTemplateField = new PageTemplateField();
        $pageTemplateField->setName('metaDescription');
        $pageTemplateField->setType(PageTemplateField::TYPE_TEXT);
        $pageTemplateField->setOptions('{"required": true}');
        $commonFields[] = $pageTemplateField;

        // metaKeywords
        $pageTemplateField = new PageTemplateField();
        $pageTemplateField->setName('metaKeywords');
        $pageTemplateField->setType(PageTemplateField::TYPE_TEXT);
        $pageTemplateField->setOptions('{"required": true}');
        $commonFields[] = $pageTemplateField;

        return $commonFields;
    }

    // Get stored field options JSON string and convert valid options to array
    private function getFieldOptions($field)
    {
        $fieldType = $field->getType();

        // BC for old symfony types saved in DB. Remove later!!
        $oldTypesMap = [
            TextType::class => PageTemplateField::TYPE_TEXT,
            TextareaType::class => PageTemplateField::TYPE_TEXTAREA,
            CKEditorType::class => PageTemplateField::TYPE_CKEDITOR,
            EntityType::class => PageTemplateField::TYPE_ENTITY,
        ];
        $oldTypes = array_keys($oldTypesMap);
        if (in_array($fieldType, $oldTypes)) {
            $fieldType = $oldTypesMap[$fieldType];
        }
        // BC END

        $fieldOptions = json_decode($field->getOptions(), true);

        switch ($fieldType) {
            case PageTemplateField::TYPE_TEXT:
                return [];
            break;
            case PageTemplateField::TYPE_TEXTAREA:
                return [
                    'attr' => [
                        'rows' => isset($fieldOptions['rows']) ? $fieldOptions['rows'] : 10,
                    ],
                ];
            break;
            case PageTemplateField::TYPE_CKEDITOR:
                return [
                    'config' => ['height' => isset($fieldOptions['height']) ? $fieldOptions['height'] : 200],
                ];
            break;
            case PageTemplateField::TYPE_ENTITY:
                return [
                    'class' => $fieldOptions['class'],
                    'placeholder' => 'Please choose an option', // Prevesti !!
                ];
            break;
            case PageTemplateField::TYPE_JSON:
                return [
                    'attr' => [
                        'rows' => isset($fieldOptions['rows']) ? $fieldOptions['rows'] : 5,
                        'placeholder' => isset($fieldOptions['placeholder']) ? $fieldOptions['placeholder'] : '{
    "line_1": "Enter valid",
    "line_2": "JSON object"
}',
                    ],
                ];
            break;
        }

        throw new \Exception(sprintf('Custom field type "%s" is not defined in class "%s"', $fieldType, __CLASS__));
    }

    // Get stored "required" field options JSON string and convert valid options to array
    private function getRequiredFieldOptions($field)
    {
        $requiredOptions = [];

        if (!empty($field->getOptions())) {
            $options = json_decode($field->getOptions(), true);
            if ($options !== null && is_array($options)) {
                foreach ($options as $option => $value) {
                    if ($option == 'required' && $value == true) {
                        $requiredOptions['constraints'] = [new NotBlank()];
                    }
                }
            }
        }

        return $requiredOptions;
    }

    private function getFieldValidatorOptions($pageTemplateField)
    {
        $fieldType = $pageTemplateField->getType();
        $validatorOptions = [];

        if ($fieldType == PageTemplateField::TYPE_JSON) {
            $validatorOptions['constraints'] = [new JsonConstraint()];
        }

        return $validatorOptions;
    }
}
