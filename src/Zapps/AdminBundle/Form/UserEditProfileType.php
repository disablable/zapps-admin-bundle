<?php

namespace Zapps\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraint\UserPassword;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Zapps\AdminBundle\Helper\UserHelper;

class UserEditProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array('label' => 'Username'))
            ->add('first_name', TextType::class, array('label' => 'First name'))
            ->add('last_name', TextType::class, array('label' => 'Last name'))
            ->add('email', 'email', array('label' => 'Email'))
        ;
        // if authenticated user is SUPER_ADMIN he can activate/deactivate users
        if($options['is_super_admin'] === true)
        {
            $builder->add('enabled', 'choice', array(
                'label' => 'Active',
                'choices' => array(
                    0 => 'No',
                    1 => 'Yes',
                ),
            ));
        }
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'is_super_admin' => false,
            'data_class' => 'Zapps\AdminBundle\Entity\User',
            'validation_groups' => array('Profile'),   // FOSUserBundle uses validation groups in constraints so we have to define which validation group we need to use here
        ));
    }

    public function getName()
    {
        return 'zapps_adminbundle_usereditprofiletype';
    }
}
