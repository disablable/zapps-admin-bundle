<?php

namespace Zapps\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Zapps\AdminBundle\Helper\PageTemplateFieldHelper;

class PageTemplateFieldType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('type', ChoiceType::class, [
                'choices' => PageTemplateFieldHelper::getTypeFormChoices(),
                'choices_as_values' => true,
            ])
            ->add('options', TextareaType::class, [
                'label' => 'Opt.',
                'attr' => [
                    'placeholder' => '{
    "line_1": "Enter valid",
    "line_2": "JSON object"
}',
                    'rows' => 4,
                    'cols' => 10,
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Zapps\AdminBundle\Entity\PageTemplateField',
            'cascade_validation' => true,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'zapps_pagetemplatefield';
    }
}
