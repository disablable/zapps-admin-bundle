<?php

namespace Zapps\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Zapps\AdminBundle\Helper\UserHelper;

class UserCreateProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array('label' => 'Username'))
            ->add('first_name', null, array('label' => 'First name'))
            ->add('last_name', null, array('label' => 'Last name'))
            ->add('email', 'email', array('label' => 'Email'))
            ->add('plain_password', 'repeated', array(
                'label' => 'Password',
                'type' => 'password',
                'first_options' => array('label' => 'Enter password'),
                'second_options' => array('label' => 'Repeat password'),
                'invalid_message' => 'The password fields must match',
            ))
            ->add('roles', 'user_roles', array(
                'label' => 'Roles',
                'multiple' => true,
                'expanded' => true,
            ))
            ->add('enabled', 'choice', array(
                'label' => 'Active',
                'choices' => array(
                    0 => 'No',
                    1 => 'Yes',
                ),
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Zapps\AdminBundle\Entity\User',
            'validation_groups' => array('Registration'),   // FOSUserBundle uses validation groups in constraints so we have to define which validation group we need to use here
        ));
    }

    public function getName()
    {
        return 'zapps_adminbundle_usercreateprofiletype';
    }
}
