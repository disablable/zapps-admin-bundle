<?php

namespace Zapps\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Zapps\AdminBundle\Validator\JsonConstraint;

class PageTemplateField
{
    const TYPE_TEXT = 'text';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_CKEDITOR = 'ckeditor';
    const TYPE_ENTITY = 'entity';
    const TYPE_JSON = 'json';

    /**
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^\w+$/",
     *     message="Name can only contain letters, numbers and underscore character."
     * )
     */
    private $name;

    private $type;

    /**
     * @JsonConstraint
     */
    private $options;

    private $value;


    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }
}
