<?php

namespace Zapps\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Zapps\AdminBundle\Entity\PageTemplateField;

/**
 * @ORM\Table(name="page_template")
 * @ORM\Entity
 */
class PageTemplate
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(name="controller", type="string", length=255)
     * @Assert\NotBlank()
     */
    private $controller;

    /**
     * @ORM\Column(name="fields", type="json_array", nullable=true)
     */
    private $fields;


    public function __construct()
    {
        $this->fields = [];
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setController($controller)
    {
        $this->controller = $controller;
        return $this;
    }

    public function getController()
    {
        return $this->controller;
    }

    //
    // Since we are not using doctrine associatiions to store fields in seperate table,
    // we have to map entity to array and reverse manualy.
    //
    public function setFields($fields)
    {
        $this->fields = [];
        foreach ($fields as $field) {
            $this->fields[] = [
                'name' => $field->getName(),
                'type' => $field->getType(),
                'options' => $field->getOptions(),
            ];
        }

        return $this;
    }

    public function getFields()
    {
        $fields = [];
        foreach ($this->fields as $field) {
            $fieldEntity = new PageTemplateField();
            $fieldEntity->setName($field['name']);
            $fieldEntity->setType($field['type']);
            $fieldEntity->setOptions(isset($field['options']) ? $field['options'] : '');
            $fields[] = $fieldEntity;
        }

        return $fields;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
