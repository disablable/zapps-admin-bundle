<?php

namespace Zapps\AdminBundle\Entity;

trait TranslatableMethods
{
    /**
     * Proxy method for getting translatable fields directly from tranlsated entity,
     * so we can do this directly:
     *     $product->getName()
     * instead of this:
     *     $product->translate('en')->getName()
     */
    public function __call($method, $args)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get'. ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $args);
    }
}
