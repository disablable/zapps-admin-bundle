<?php

namespace Zapps\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Entity\User as BaseUser;

/**
 * Zapps\AdminBundle\Entity\User
 *
 * @ORM\Table(name="user_user")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $firstName
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string $lastName
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;


    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    // This overrides FOS\UserBundle\Model\user::setUsername() method to force $username to be a string.
    // This fixes wierd error showing in user edit form when trying to save empty username field.
    // Instead of showing error, it throws exception "Username must be a string, or object should have method: getUsername"
    public function setUsername($username)
    {
        $this->username = (string)$username;

        return $this;
    }

    /**
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return sting
     */
    public function getFullName()
    {
        return trim(sprintf("%s %s", $this->firstName, $this->lastName));
    }
}
