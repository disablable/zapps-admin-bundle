<?php

namespace Zapps\AdminBundle\Grid\Filter;

use Zapps\AdminBundle\Grid\Common\AbstractFilter;

class EntityFilter extends AbstractFilter
{
    public function getType()
    {
        return 'entity';
    }

    public function setOptions(array $options = array())
    {
        if(isset($options['class']))
            parent::setOptions(array('class' => $options['class']));
    }
}