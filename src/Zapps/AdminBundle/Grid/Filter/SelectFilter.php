<?php

namespace Zapps\AdminBundle\Grid\Filter;

use Zapps\AdminBundle\Grid\Common\AbstractFilter;

class SelectFilter extends AbstractFilter
{
    public function getType()
    {
        return 'select';
    }

    public function setOptions(array $options = array())
    {
        if(isset($options['choices']))
            parent::setOptions(array('choices' => $options['choices']));
    }
}