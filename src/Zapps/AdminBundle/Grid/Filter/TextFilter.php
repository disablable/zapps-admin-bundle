<?php

namespace Zapps\AdminBundle\Grid\Filter;

use Zapps\AdminBundle\Grid\Common\AbstractFilter;

class TextFilter extends AbstractFilter
{
    public function getType()
    {
        return 'text';
    }
}