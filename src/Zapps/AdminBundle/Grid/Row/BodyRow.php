<?php

namespace Zapps\AdminBundle\Grid\Row;

use Zapps\AdminBundle\Grid\Common\AbstractRow;

class BodyRow extends AbstractRow
{
    public function getType()
    {
        return 'body';
    }
}