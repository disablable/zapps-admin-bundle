<?php

namespace Zapps\AdminBundle\Grid\Row;

use Zapps\AdminBundle\Grid\Common\AbstractRow;

class HeaderRow extends AbstractRow
{
    public function getType()
    {
        return 'header';
    }
}