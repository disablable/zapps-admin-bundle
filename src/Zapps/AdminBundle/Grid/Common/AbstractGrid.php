<?php

namespace Zapps\AdminBundle\Grid\Common;

use Symfony\Component\Form\Form;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\EntityManager;

use Zapps\AdminBundle\Grid\Common\AbstractColumn;
use Zapps\AdminBundle\Grid\Common\AbstractFilter;

abstract class AbstractGrid
{
    private $options = array();

    private $columns = array();

    private $filters = array();

    private $gridEntity;

    private $form;

    private $rows = array();

    abstract public function buildGrid(EntityManager $em);

    abstract public function getBlockPrefix();

    abstract public function getEntityName();

    abstract public function getRoutes();

    public function __construct(array $options = array())
    {
        $this->setOptions($options);
    }

    /**
     * Sets given options to grid.
     *
     * @param array $options Array of grid options. Possible options: "results_per_page".
     * @throws \Exception
     */
    public function setOptions(array $options = array())
    {
        if (isset($options['results_per_page'])) {
            if ($options['results_per_page'] < 1) {
                throw new \Exception(sprintf('Invalid results_per_page value "%s"', $options['results_per_page']));
            }
        }

        $this->options = $options;
    }

    public function getOption($option)
    {
        return isset($this->options[$option]) ? $this->options[$option] : false;
    }

    public function addColumn(AbstractColumn $column)
    {
        $this->columns[$column->getName()] = $column;

        return $this;
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function addFilter(AbstractFilter $filter)
    {
        $this->filters[$filter->getName()] = $filter;

        return $this;
    }

    public function getFilters()
    {
        return $this->filters;
    }

    public function setEntity(GridEntity $gridEntity)
    {
        $this->gridEntity = $gridEntity;
    }

    public function getGridEntity()
    {
        return $this->gridEntity;
    }

    public function setForm(Form $form)
    {
        $this->form = $form;
    }

    public function getForm()
    {
        return $this->form;
    }

    public function setRows(Paginator $rows)
    {
        $this->rows = $rows;
    }

    public function getRows()
    {
        return $this->rows;
    }

    public function getGridData($em, $entityName, array $columns, array $filters, array $sort, array $limit)
    {
        $queryParams = [];

        $dql = "
            SELECT entity
            FROM {$entityName} entity
            WHERE 1 = 1
        ";

        // filters
        foreach ($filters as $filter) {
            if ($filter['value'] !== null) {
                switch ($filter['type']) {
                    case 'text':
                        $dql .= sprintf(" AND entity.%s LIKE :filter_%s", $filter['name'], $filter['name']);
                        $queryParams['filter_'.$filter['name']] = '%'.$filter['value'].'%';
                        break;
                    case 'select':
                        $dql .= sprintf(" AND entity.%s LIKE :filter_%s", $filter['name'], $filter['name']);
                        $queryParams['filter_'.$filter['name']] = '%'.$filter['value'].'%';
                        break;
                    case 'entity':
                        $dql .= sprintf(" AND entity.%s LIKE :filter_%s", $filter['name'], $filter['name']);
                        $queryParams['filter_'.$filter['name']] = '%'.$filter['value'].'%';
                        break;
                }
            }
        }

        // sort
        if (isset($sort['column']) && isset($sort['direction'])) {
            $dql .= sprintf(" ORDER BY entity.%s %s", $sort['column'], $sort['direction']);
        }

        $query = $em->createQuery($dql);

        // params
        foreach ($queryParams as $paramName => $paramValue) {
            $query->setParameter($paramName, $paramValue);
        }

        // limit
        if (isset($limit['offset']) && isset($limit['max_results'])) {
            $query->setFirstResult($limit['offset']);
            $query->setMaxResults($limit['max_results']);
        }

        $data = new Paginator($query);

        return $data;
    }
}
