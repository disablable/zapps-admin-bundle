<?php

namespace Zapps\AdminBundle\Grid\Common;

abstract class AbstractRow
{
    private $options = array();

    private $data;

    public function __construct($data, array $options = array())
    {
        $this->data = $data;
        $this->options = $options;
    }

    abstract public function getType();
    
    public function setOptions($options)
    {
        if(isset($options['selected']))
            $this->options['selected'] = $options['selected'];
    }

    public function getOptions()
    {
        return $this->options;
    }
    
    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}