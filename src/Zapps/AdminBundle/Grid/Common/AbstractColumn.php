<?php

namespace Zapps\AdminBundle\Grid\Common;

abstract class AbstractColumn
{
    const SORT_DIRECTION_UP = 'up';
    const SORT_DIRECTION_DOWN = 'down';

    private $name;

    protected $options = [
        'label' => null,            // column heading label
        'sortable' => false,        // is column sortable
        'alignment' => 'center',    // column alignment
    ];

    private $sortDirection = null;

    public function __construct($name, array $options = [])
    {
        $this->name = $name;
        $this->options = array_merge($this->options, $options);
    }

    abstract public function getType();

    public function getAlignment()
    {
        return !empty($this->options['alignment'])
            ? $this->options['alignment']
            : 'center';
    }

    public function getLabel()
    {
        return isset($this->options['label'])
            ? $this->options['label']
            : ucfirst(preg_replace('/(?<!^)([A-Z])/', ' \\1', $this->getName()));
    }

    public function getName()
    {
        return $this->name;
    }

    public function isSortable()
    {
        return $this->options['sortable'];
    }

    public function setSortDirection($direction)
    {
        $this->sortDirection = $direction;
    }

    public function getSortDirection()
    {
        return $this->sortDirection;
    }
}
