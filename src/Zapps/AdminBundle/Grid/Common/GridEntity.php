<?php

namespace Zapps\AdminBundle\Grid\Common;

class GridEntity
{
    private $page;

    private $sortColumn;

    private $sortDirection;

    private $filters;

    private $selected = array();

    public function __construct()
    {
        $this->setPage(1);
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setSortColumn($sortColumn)
    {
        $this->sortColumn = $sortColumn;
    }

    public function getSortColumn()
    {
        return $this->sortColumn;
    }

    public function setSortDirection($sortDirection)
    {
        $this->sortDirection = $sortDirection;
    }

    public function getSortDirection()
    {
        return $this->sortDirection;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    public function getFilters()
    {
        return $this->filters;
    }

    public function setSelected($selected)
    {
        $this->selected = $selected;
    }

    public function getSelected()
    {
        return $this->selected;
    }
}
