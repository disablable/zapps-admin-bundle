<?php

namespace Zapps\AdminBundle\Grid\Common;

abstract class AbstractFilter
{
    private $name;

    protected $options = array(
        'required' => false,
    );

    abstract public function getType();

    public function __construct($name, array $options = array())
    {
        $this->setName($name);
        $this->setOptions($options);
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setOptions(array $options = array())
    {
        $this->options = array_merge($this->options, $options);
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function createRepositoryData($value)
    {
        return array(
            'name' => $this->getName(),
            'type' => $this->getType(),
            'value' => $value,
        );
    }
}