<?php

namespace Zapps\AdminBundle\Grid\Column;

use Zapps\AdminBundle\Grid\Common\AbstractColumn;

class DateColumn extends AbstractColumn
{
    public function getType()
    {
        return 'date';
    }
}
