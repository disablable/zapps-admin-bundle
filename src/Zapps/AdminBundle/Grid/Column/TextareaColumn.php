<?php

namespace Zapps\AdminBundle\Grid\Column;

use Zapps\AdminBundle\Grid\Common\AbstractColumn;

class TextareaColumn extends AbstractColumn
{
    public function getType()
    {
        return 'textarea';
    }
}
