<?php

namespace Zapps\AdminBundle\Grid\Column;

use Zapps\AdminBundle\Grid\Common\AbstractColumn;

class MoneyColumn extends AbstractColumn
{
    public function getType()
    {
        return 'money';
    }
}
