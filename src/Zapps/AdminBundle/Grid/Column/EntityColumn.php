<?php

namespace Zapps\AdminBundle\Grid\Column;

use Zapps\AdminBundle\Grid\Common\AbstractColumn;

class EntityColumn extends AbstractColumn
{
    protected $options = [
        'property_name' => null,
    ];

    public function getType()
    {
        return 'entity';
    }

    public function getPropertyName()
    {
        return $this->options['property_name'];
    }
}
