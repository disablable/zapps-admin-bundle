<?php

namespace Zapps\AdminBundle\Grid\Column;

use Zapps\AdminBundle\Grid\Common\AbstractColumn;

class TextColumn extends AbstractColumn
{
    public function getType()
    {
        return 'text';
    }
}
