<?php

namespace Zapps\AdminBundle\Grid;

use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\ORM\EntityManager;

use Zapps\AdminBundle\Grid\Common\AbstractGrid;
use Zapps\AdminBundle\Grid\Column\TextColumn;
use Zapps\AdminBundle\Grid\Filter\TextFilter;
use Zapps\AdminBundle\Helper\UserHelper;

class UserProfileGrid extends AbstractGrid
{
    public function buildGrid(EntityManager $em)
    {
        $this
            ->addColumn(new TextColumn('id', ['sortable' => true]))
            ->addColumn(new TextColumn('username', ['sortable' => true]))
            ->addColumn(new TextColumn('email', ['sortable' => true]))
            ->addColumn(new TextColumn('firstName', ['sortable' => true]))
            ->addColumn(new TextColumn('lastName', ['sortable' => true]))

            // ->addFilter(new TextFilter('username'))
            // ->addFilter(new TextFilter('email'))
            // ->addFilter(new TextFilter('firstName'))
            // ->addFilter(new TextFilter('lastName'))
        ;
    }

    public function getBlockPrefix()
    {
        return 'user_profile';
    }

    public function getEntityName()
    {
        return 'ZappsAdminBundle:User';
    }

    public function getGridData($em, $entityName, array $columns, array $filters, array $sort, array $limit)
    {
        $queryParams = [];

        $dql = "
            SELECT entity
            FROM {$entityName} entity
            WHERE 1 = 1
        ";

        // if authenticated user is not SUPER_ADMIN, don't list users with roles ROLE_SUPER_ADMIN and ROLE_ADMIN
        if(!in_array(UserHelper::ROLE_SUPER_ADMIN, $this->getOption('auth_user_roles')))
        {
            $dql .= " AND entity.roles NOT LIKE :role_super_admin";
            $dql .= " AND entity.roles NOT LIKE :role_admin";

            $queryParams['role_super_admin'] = '%ROLE_SUPER_ADMIN%';
            $queryParams['role_admin'] = '%ROLE_ADMIN%';
        }

        // filters
        foreach ($filters as $filter) {
            if ($filter['value'] != null) {
                switch ($filter['type']) {
                    case 'text':
                        $dql .= sprintf(" AND entity.%s LIKE :filter_%s", $filter['name'], $filter['name']);
                        $queryParams['filter_'.$filter['name']] = '%'.$filter['value'].'%';
                        break;
                }
            }
        }

        // sort
        if (isset($sort['column']) && isset($sort['direction'])) {
            $dql .= sprintf(" ORDER BY entity.%s %s", $sort['column'], $sort['direction']);
        }

        $query = $em->createQuery($dql);

        // params
        foreach ($queryParams as $paramName => $paramValue) {
            $query->setParameter($paramName, $paramValue);
        }

        // limit
        if (isset($limit['offset']) && isset($limit['max_results'])) {
            $query->setFirstResult($limit['offset']);
            $query->setMaxResults($limit['max_results']);
        }

        $data = new Paginator($query);

        return $data;
    }

    public function getRoutes()
    {
        return [
            'list' => 'admin_user_profile_list',
            'new' => 'admin_user_profile_new',
            'edit' => 'admin_user_profile_edit',
            'delete' => 'admin_user_profile_delete',
        ];
    }
}
