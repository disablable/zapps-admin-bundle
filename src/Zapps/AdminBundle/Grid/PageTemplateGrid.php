<?php

namespace Zapps\AdminBundle\Grid;

use Doctrine\ORM\EntityManager;

use Zapps\AdminBundle\Grid\Common\AbstractGrid;
use Zapps\AdminBundle\Grid\Column\TextColumn;

class PageTemplateGrid extends AbstractGrid
{
    public function buildGrid(EntityManager $em)
    {
        $this
            ->addColumn(new TextColumn('id', ['sortable' => true]))
            ->addColumn(new TextColumn('name', ['sortable' => true]))
        ;
    }

    public function getBlockPrefix()
    {
        return 'page_template';
    }

    public function getEntityName()
    {
        return 'ZappsAdminBundle:PageTemplate';
    }

    public function getRoutes()
    {
        return [
            'list' => 'admin_page_template_list',
            'new' => 'admin_page_template_new',
            'edit' => 'admin_page_template_edit',
            'delete' => 'admin_page_template_delete',
        ];
    }
}
