/**
 * container.js
 */
zApps.container = function(params) {
	var self = this;
	var windowHolder = null;
	var options = {
		id: null,
        zIndex: 2000
	};

	self.getId = function() {
		return options.id;
	};

	self.getWindowHolder = function() {
		return windowHolder;
	};

    self.getZIndex = function() {
        return options.zIndex;
    };

    self.showLoader = function() {
    	var container = self.getWindowHolder();
    	$(container).append('<div class="modal" tabindex="-1" style="display: block; padding-right: 15px;"><div style=""><div class="overlay loder-overlay" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 24px;"><i class="fa fa-refresh fa-spin"></i></div></div></div>');
    	return true;
    };

	(function(params) {
		// merge params with options
		if(params === undefined || params === null || params === false)
			params = {};
		options = $.extend(options, params);

		// check if id is set
		if(options.id === null || options.id == '')
			throw "Container id is not defined!";

		// define holder for modal windows
		windowHolder = $('#' + options.id);
	})(params);
};
