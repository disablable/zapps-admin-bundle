/**
 * winManager.js
 */

zApps.winManager = function() {
	var self = this;
	var windows = [];
	var initStack = [];

	self.getWindows = function() {
		return windows;
	};

	self.getInitStack = function() {
		return initStack;
	};

	self.addToInitStack = function(newFunction) {
		initStack.push(newFunction);
	};

	// ! This function is not in use, because there can be two identical function on stack but referencing different elements.
	// So we are allowing the same function to be on stack at once !
	// self.initStackHas = function(newFunction) {
	// 	var stackHas = false;

	// 	$.each(initStack, function(index, value) {
	// 		if(String(value) === String(newFunction)) {
	// 			stackHas = true;
	// 		}
	// 	});
	// 	return stackHas;
	// };

	// create window
	self.createWindow = function (options) {
		// if window already exists (trying to create duplicate window)
		if(options.id in windows) {
			// get existing window
			var existingWindow = windows[options.id];

			if(existingWindow.getOnDuplicate() == 'reload') {
				// reload window data with new source URL
				existingWindow.load(options.sourceUrl);
			}
			else if(existingWindow.getOnDuplicate() == 'highlight') {
				// highlight existing window
				existingWindow.highlight();
			}

			// return existing window
			return existingWindow;
		}
		else {
			var newWindow = null;

			// create new window
			switch(options.type) {
				case 'dialog':
					newWindow = new zApps.dialogWindow( {
						id: options.id,
						title: options.title,
						sourceUrl: options.sourceUrl,
						data: options.data,
						parent: options.parent,
						onOpen: options.onOpen,
						onClose: options.onClose,
						onDuplicate: options.onDuplicate,
                        modal: options.modal,
						width: options.width,
						height: options.height,
						position: options.position,
						resizable: options.resizable,
						draggable: options.draggable,
                        zIndex: options.parent.getZIndex() + 2
					});
					break;

				default: case 'static':
					newWindow = new zApps.staticWindow( {
						id: options.id,
						title: options.title,
						sourceUrl: options.sourceUrl,
						data: options.data,
						parent: options.parent,
						onOpen: options.onOpen,
						onClose: options.onClose,
						onDuplicate: options.onDuplicate,
                        //zIndex: options.parent.getZIndex() + 2
					});
					break;
			}

			// save window
			windows[options.id] = newWindow;

			// open window
			newWindow.open();

			// return window
			return newWindow;
		}
	};

	// destroy window
	self.destroyWindow = function (id) {
		// if window exists
		if(id in windows) {

			// close window if opened
			//if(windows[id].isOpen() === true)
			//	windows[id].close();

			// delete window
			delete windows[id];
		}
	};

	// return window by id
	self.getWindow = function (id) {
		return windows[id];
	};

	// constructor
	(function() {})();
};