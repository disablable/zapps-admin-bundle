/**
 * layout.js
 */
zApps.layout = function(params) {
	var self = this;
	var windowHolder = null;
	var options = {
		id: null
	};
	
	self.getId = function() {
		return options.id;
	};
	
	self.getWindowHolder = function() {
		return windowHolder;
	};
	
	(function(params) {
		// merge params with options
		if(params === undefined || params === null || params === false)
			params = {};
		options = $.extend(options, params);
		
		// check if id is set
		if(options.id === null || options.id == '')
			throw "Layout id not defined!";
		
		// create holder for window
		windowHolder = $('<div id="' + options.id + '"></div>');
		
		windowHolder.appendTo($('#main_container'));
	})(params);
};