/**
 * translationWidget.js
 */

zApps.translationWidget = function(params) {
    var self = this;
    var options = {
        parentWindow: null,
        translationsHolder: null
    };
    var $translationAddLink = null;

    self.loadTranslation = function(localeVal, contentVal) {
        // create translation prototype
        var $translationPrototype = self._createTranslationPrototype();

        self._addTranslation(localeVal, contentVal);
    };

    self._bindTranslationAddLink = function() {
        // If there are no more locales to be added (they are all in form), then disable "Add translation" button
        var $translationPrototype = self._createTranslationPrototype();
        if ($translationPrototype.find('select[name$="[locale]"] > option').length == 0) {
            $translationAddLink.prop('disabled', true);
        }

        $translationAddLink.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            self._addTranslation();
        });
    };

    self._createTranslationPrototype = function() {
        var prototypeHtml = options.translationsHolder.data('prototype');

        // get highest index number from translation widgets
        var index = 0;
        options.translationsHolder.parents('form').find('div[name="translation-widget"]').each(function(key, value) {
            var $translationWidget = $(value);
            var currentWidgetIndex = $translationWidget.data('translation-index');
            if(currentWidgetIndex > index) {
                index = currentWidgetIndex;
            }
        });
        // increase index by 1
        index++;

        // replace index holder with index
        prototypeHtml = prototypeHtml.replace(/__name__/g, index);

        // create jQuery object
        var $prototype = $(prototypeHtml);

        // find already translated locales in given translations holder
        var translatedLocales = self._findTranslatedLocales(options.translationsHolder);

        // remove locales from prototype that are already translated
        $.each(translatedLocales, function(index, value) {
            $prototype.find('select[name$="[locale]"] > option[value="' + value + '"]').remove();
        });

        return $prototype;
    };

    self._bindTranslationRemoveLink = function($translationWidget) {
        // find remove link
        var $translationRemoveLink = $translationWidget.find('button[name="remove-translation"]');

        $translationRemoveLink.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // remove translation widget from form
            $(this).parents('div[name="translation-widget"]').remove();

            // If you removed one ove the translation widgets, that means that there is place to add it again,
            // so reenable "Add translation" button
            $translationAddLink.prop('disabled', false);
        });
    };

    self._bindTranslationRemoveLinks = function() {
        // add translation remove links to each translation widget
        options.translationsHolder.find('div[name="translation-widget"]').each(function(index, value) {
            var translationWidget = $(value);

            self._bindTranslationRemoveLink(translationWidget);
        });
    };

    self._addTranslation = function(localeVal, contentVal) {
        // create translation prototype
        var $translationPrototype = self._createTranslationPrototype();

        // Disable locale choices for all previously added locales
        // Since select doesn't have readonly option, we replace it with custom readonlySelect element
        options.translationsHolder.find('select[name$="[locale]"]').each(function(key, val) {
            var $selectElement = $(val);

            // Add readonly select element instead
            var id = $selectElement.attr('id');
            var name = $selectElement.attr('name');
            var selectedValue = $selectElement.val();
            var selectedLabel = $selectElement.find('option:selected').text();
            var readonlySelect2 = self._getReadonlySelectElement(id, name, selectedValue, selectedLabel);

            $selectElement.after(readonlySelect2);

            // Remove original select element
            $selectElement.select2("destroy");
            $selectElement.remove();
        });

        if (localeVal) {
            // Select loaded locale
            $translationPrototype.find('[name$="[locale]"]').val(localeVal);
        }

        if (contentVal) {
            // Insert loaded content for that locale
            $translationPrototype.find('[name$="[content]"]').val(contentVal);
        }

        // add prototype widget to form
        $translationAddLink.before($translationPrototype);

        // Initiate select2 widget
        $translationPrototype.find('select').select2({
            dropdownParent: $("#" + options.parentWindow.getId()).find('.modal-body'),
            minimumResultsForSearch: "Infinity"
        });

        // bind remove link for added prototype widget
        self._bindTranslationRemoveLink($translationPrototype);

        // If there is no more locales to add (they are all added in form), disable "Add translation button"
        if ($translationPrototype.find('select[name$="[locale]"] > option').length <= 1) {
            $translationAddLink.prop('disabled', true);
        }
    };

    self._findTranslatedLocales = function() {
        translatedLocales = [];
        // First find if there are already trans widgets for some locale:
        options.translationsHolder.find('div[name="translation-widget"]').each(function(key, value) {
            // Add existing locale value to array
            translatedLocales.push($(value).find('[name$="[locale]"]').val());
        });

        return translatedLocales;
    };

    self._getReadonlySelectElement = function(id, name, value, label) {
        var template = '<span class="select2 select2-container select2-container--default select2-container--disabled" dir="ltr" style="width: 60px;">'
                     + '    <span class="selection">'
                     + '        <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="-1" aria-labelledby="select2-620t-container">'
                     + '            <span class="select2-selection__rendered" id="select2-620t-container" title="'+ label +'">'+ label +'</span>'
                     + '            <span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>'
                     + '        </span>'
                     + '    </span>'
                     + '    <span class="dropdown-wrapper" aria-hidden="true"></span>'
                     + '</span>'
                     + '<input type="hidden" class="form-control" id="'+ id +'" name="'+ name +'" value="'+ value +'" />';

        return template;
    };

    // constructor
    (function(params) {
        // merge params with options
        if(params === undefined || params === null || params === false)
            params = {};
        options = $.extend(options, params);

        // check if translationsHolder is set
        if(options.parentWindow === null || options.parentWindow == '')
            throw "Parent window not defined!";

        // check if translationsHolder is set
        if(options.translationsHolder === null || options.translationsHolder == '')
            throw "Translations holder not defined!";

        // translation add button
        $translationAddLink = options.translationsHolder.find('button[name="add-translation"]');

        self._bindTranslationAddLink();
        self._bindTranslationRemoveLinks();

    })(params);
};
