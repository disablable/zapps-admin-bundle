/**
 * zApps translation js
 * !!! Premjestiti poslije u bundle !!!
 */
function bindTranslationsAddLink($translationsAddLink, $translationsHolder) {
    $translationsAddLink.on('click', function(e){
        // prevent the link from creating a "#" on the URL
        e.preventDefault();
        
        // create translation prototype
        var $translationPrototype = createTranslationPrototype($translationsHolder);
        
        // add prototype widget to form
        $translationsHolder.append($translationPrototype);
        
        // add remove link for added prototype widget
        activateTranslationDeleteLink($translationPrototype);
    });
}

/*function findTranslatedLocales($translationsHolder) {
    
    // find already translated locales in form
    var translatedLocales = [];
    $translationsHolder.find('select[name*="[locale]"]').each(function(index, value) {
        translatedLocales.push($(value).find('option:selected').val());
    });
    
    return translatedLocales;
}*/

function createTranslationPrototype($translationsHolder)
{
    var prototypeHtml = $translationsHolder.data('prototype');
    
    // total number of translation widgets inside of the same form
    var index = $translationsHolder.parent('form').find('.zApps-translation-widget').length;
    
    // replace index holder with index
    prototypeHtml = prototypeHtml.replace(/__name__/g, index);
    
    // create jQuery object
    var $prototype = $(prototypeHtml);
    
    // find already translated locales in given translations holder
    //var translatedLocales = findTranslatedLocales($translationsHolder);
    
    // remove locales from prototype that are already translated
    /*$.each(translatedLocales, function(index, value) {
        $prototype.find('select > option[value="' + value + '"]').remove();
    });*/
    
    return $prototype;
}

function activateTranslationDeleteLinks($translationsHolder) {
    
    // add delete link on each translation
    $translationsHolder.find('.zApps-translation-widget').each(function(index, value) {
        var $translationWidget = $(value);
        
        activateTranslationDeleteLink($translationWidget);
    });
}

function activateTranslationDeleteLink($translationWidget) {
    
    // find delete link
    var $deleteLink = $translationWidget.find('.zApps-translation-remove');

    $deleteLink.on('click', function(e){
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // remove translation widget from form
        $(this).parent('.zApps-translation-widget').remove();
    });
}