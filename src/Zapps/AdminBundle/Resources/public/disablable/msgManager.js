/**
 * msgManager.js
 */

zApps.msgManager = function() {
	var self = this;
	
	self.showMessages = function(messages) {
		if(messages.length > 0) {
			$.each(messages, function(index, message) {
				if(message.type == 'error')
					self.showErrorMessage(message.text);
				else if(message.type == 'warning')
					self.showWarningMessage(message.text);
				else if(message.type == 'success')
					self.showSuccessMessage(message.text);
			});
		}
	};
	
	self.showSuccessMessage = function(successMessage) {
		$.pnotify( {
			pnotify_type: "notice",
			pnotify_title: "Sucess",
			pnotify_animate_speed: 500,	// ms
			pnotify_delay: 5000,	// ms
			pnotify_text: successMessage,
			pnotify_hide: true,
			pnotify_sticker: false,
			pnotify_closer_hover: false,
			pnotify_shadow: true,
			pnotify_nonblock: true,
			pnotify_nonblock_opacity: 0.2,
			pnotify_before_open: function(pnotify) {
				pnotify.css( {
					left: $(window).width() / 2 - pnotify.width() / 2
				});
			}
		});
	};
	
	self.showWarningMessage = function(warningMessage) {
		$.pnotify( {
			pnotify_type: "notice",
			pnotify_title: "Warning",
			pnotify_animate_speed: 500,	// ms
			pnotify_delay: 5000,	// ms
			pnotify_text: warningMessage,
			pnotify_hide: true,
			pnotify_sticker: false,
			pnotify_closer_hover: false,
			pnotify_shadow: true,
			pnotify_nonblock: true,
			pnotify_nonblock_opacity: 0.2,
			pnotify_before_open: function(pnotify) {
				pnotify.css( {
					left: $(window).width() / 2 - pnotify.width() / 2
				});
			}
		});
	};
	
	self.showErrorMessage = function(errorMessage) {
		$.pnotify( {
			pnotify_type: "error",
			pnotify_title: "Error",
			pnotify_animate_speed: 500,	// ms
			pnotify_delay: 5000,	// ms
			pnotify_text: errorMessage,
			pnotify_hide: true,
			pnotify_sticker: false,
			pnotify_closer_hover: false,
			pnotify_shadow: true,
			pnotify_nonblock: true,
			pnotify_nonblock_opacity: 0.2,
			pnotify_before_open: function(pnotify) {
				pnotify.css( {
					left: $(window).width() / 2 - pnotify.width() / 2
				});
			}
		});
	};
	
	self.showMessage = function(type, text) {
		
	};
	
	// constructor
	(function() {
		
	})();
};