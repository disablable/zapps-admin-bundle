/**
 * staticWindow.js
 */
zApps.staticWindow = function(params) {
	var self = this;
	var windowHolder = null;
	var titleHolder = null;
	var contentHolder = null;
	var options = {
		id: null,
		title: '',
		sourceUrl: null,
		data: null,
		parent: null,
		onOpen: function() {},
		onClose: function() {},
		onDuplicate: 'highlight',	// highlight or reload
        zIndex: 500
	};

	self.getId = function() {
		return options.id;
	};

	self.getWindowHolder = function() {
		return windowHolder;
	}

	self.getParent = function() {
		return options.parent;
	}

	self.getOnDuplicate = function() {
		return options.onDuplicate;
	};

    self.getZIndex = function() {
        return options.zIndex;
    };

	self.setTitle = function(newTitle) {
		titleHolder.html(newTitle);
	};

	self.setContent = function(newContent) {
		contentHolder.html(newContent);
	};

	self.open = function() {
		/*
		// if parent window is set, get it's holder
		if(options.parent && typeof(options.parent) === 'object') {
			var parentWindowHolder = options.parent.getWindowHolder();
		}
		// else set #main_container as parent holder
		else {
			var parentWindowHolder = $('#main-container');
		}

		// create static window
		var template = '';
		template += '<div id="' + options.id + '">';
        template += '   <div class="modal-header">';
        template += '       <h3>' + options.title + '</h3>';
        template += '   </div>';
        template += '   <div class="modal-body static-window">';
        template += '       <p></p>';
        template += '   </div>';
        template += '</div>';

		// define window holder
		windowHolder = $(template);

        // set z-index
        windowHolder.css('zIndex', options.zIndex + 1);

		// define title holder
		titleHolder = windowHolder.find('.modal-header h3');

		// define content holder
		contentHolder = windowHolder.find('.modal-body p');

		windowHolder.appendTo(parentWindowHolder);

		// load window data
		self.load();

		// trigger onOpen event
		self.onOpen();*/
	};

	self.load = function(newSourceUrl, newData, submitType) {
		var data = null;

		// if source URL is given as param and is valid, use it
		if(newSourceUrl && typeof(newSourceUrl) === 'string' && newSourceUrl != '')
			options.sourceUrl = newSourceUrl;

		// if data is given as param use it
		if(newData && typeof(newData) === 'string' && newData != '')
			data = newData;
		else if(options.data && typeof(options.data) === 'object')
			data = options.data;

		if(!(options.sourceUrl === null)) {
			$.ajax( {
				url: options.sourceUrl,
				type: submitType ? submitType : 'GET',
				dataType: 'html',
				data: data,
				async: false,
				beforeSend: function() {
					self.showLoader();
				},
				success: function(responseData) {	// on success ajax response
					// put response data in content holder
					self.setContent(responseData);

					$.each(zApps.winManager.getInitStack(), function(index, value) {
						// take first init function from initStack, run it and remove it from stack
						var initFunction = zApps.winManager.getInitStack().shift();

						if(!(typeof(initFunction) == 'undefined')) {
							// call returned init function
							initFunction(self);
						}
					});

					self.hideLoader();
				},
				error: function(jqXHR, textStatus, errorThrown) {	// on error ajax response
					self.setContent('Error loading content. ' + errorThrown + '!');

					self.hideLoader();
				}
			});
		}
	};

	self.showLoader = function() {
		var loader = $('<div class="zApps-loader"><img alt="Loading..." src="' + zApps.vars.ajaxLoader + '" /></div>');
		//var topPosition = (contentHolder.height() / 2) - 12 + 38;
		//var leftPosition = (contentHolder.width() / 2) - 12;
		//loader.css('top', topPosition);
		//loader.css('left', leftPosition);

        loader.css('position', 'absolute');
        loader.css('top', '44%');
        loader.css('left', '44%');

		windowHolder.append(loader);

        titleHolder.fadeTo(0, 0.3);
		contentHolder.fadeTo(0, 0.3);
	};

	self.hideLoader = function() {
        titleHolder.fadeTo(0, 1);
		contentHolder.fadeTo(0, 1);
		windowHolder.find('.zApps-loader').remove();
	};

	self.highlight = function() {
		windowHolder.effect('highlight', {color: '#FF6140'}, 200);
		windowHolder.effect('highlight', {color: '#FF6140'}, 200);
	};

	self.onOpen = function() {
		options.onOpen(self);
	};

	self.onClose = function(params) {
		options.onClose(self, params);
	};

	self.isOpen = function() {
		if(windowHolder.html().length > 0)
			return true;

		return false;
	};

	self.close = function(params) {
		params = $.extend({}, params);

		// clear holder
		windowHolder.html('');

		// remove holder
		windowHolder.remove();

		// destroy window
		zApps.winManager.destroyWindow(options.id);

        // trigger onClose event
		self.onClose(params);
	};

	(function(params) {
		// merge params with options
		if(params === undefined || params === null || params === false)
			params = {};
		options = $.extend(options, params);

		// check if id is set
		if(options.id === null || options.id == '')
			throw "Window id not defined!";

	})(params);
};