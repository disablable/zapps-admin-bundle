/**
 * dialogWindow.js
 */
zApps.dialogWindow = function(params) {
	var self = this;
	var windowHolder = null;
	var contentHolder = null;
    var blurElement = null;
	var options = {
		id: null,
		sourceUrl: null,
		data: null,
		parent: null,
		onOpen: function() {},
		onClose: function() {},
        onDuplicate: 'highlight',	// highlight or reload
		modal: false,
		width: null,  // large, medium, small
		height: 'auto',
		position: 'center',
        zIndex: 1000
	};

	self.getId = function() {
		return options.id;
	};

	self.getWindowHolder = function() {
		return windowHolder;
	}

	self.getParent = function() {
		return options.parent;
	}

	self.getOnDuplicate = function() {
		return options.onDuplicate;
	};

    self.getZIndex = function() {
        return options.zIndex;
    };

	self.open = function() {
		// if parent window is set but it is not <body>, then get it's holder
		if(options.parent && typeof(options.parent) === 'object' && options.parent.getId() != 'body') {
			var parentWindowHolder = options.parent.getWindowHolder();
            blurElement = parentWindowHolder.find('div.modal-dialog');
		}
		// else set "window.body" as parent holder
		else {
			var parentWindowHolder = $('body');
            // If body element is parent holder, then set div with class "wrapper" as blurElement
            blurElement = $('body > div.wrapper');
		}
        // create dialog window
        var template = ''
                + '<div id="' + options.id + '" class="modal" tabindex="-1" style="display: block; padding-right: 15px;">'
                + '    <div class="modal-dialog"><!-- Add "modal-sm", "modal-lg", or no class for normal sized window -->'
                + '        <div class="modal-content box">'

                // + '            <div class="modal-header">'
                // + '                <button type="button" class="close"><span>×</span></button>'
                // + '                <h4 class="modal-title">' + options.title + '</h4>'
                // + '            </div>'
                + '            <div class="modal-body" style="height:80px;">'
                + '            </div>'
                // + '            <div class="modal-footer">'
                // + '                <button type="button" class="btn btn-default">Close</button>'
                // + '                <button type="button" class="btn btn-primary">Save changes</button>'
                // + '            </div>'

                + '        </div>'
                + '    </div>'
                + '</div>';

		// define window holder
		windowHolder = $(template);

        // set z-index
        windowHolder.css('zIndex', options.zIndex + 1);

        // define content holder
		contentHolder = windowHolder.find('.modal-content');

        // window width
        if(options.width) {
            // large
            if(options.width == 'large') {
                windowHolder.find('.modal-dialog').addClass('modal-lg');
            }
            // small
            else if (options.width == 'small') {
                windowHolder.find('.modal-dialog').addClass('modal-sm');
            }
            // if width is medium or not specified, modal will be medium sized
        }

        blurElement.addClass('blurred');

        // add window to parent window holder
		windowHolder.appendTo(parentWindowHolder);

        // load window data
		self.load();

        // trigger onOpen event
		self.onOpen();
	};

	self.load = function(newSourceUrl, newData, submitType) {
		var sourceUrl = null;
		var data = null;

		// if source URL is given as param and is valid, use it
		if(newSourceUrl && typeof(newSourceUrl) === 'string' && newSourceUrl != '')
			sourceUrl = newSourceUrl;
		// else use source URL defined in options if valid
		else if(options.sourceUrl && typeof(options.sourceUrl) === 'string')
			sourceUrl = options.sourceUrl;

		// if data is given as param use it
		if(newData && typeof(newData) === 'string' && newData != '')
			data = newData;
		else if(options.data && typeof(options.data) === 'object')
			data = options.data;

		if(!(sourceUrl === null)) {
			$.ajax( {
				url: sourceUrl,
				type: submitType ? submitType : 'GET',
				dataType: 'html',
				data: data,
				beforeSend: function() {
					self.showLoader();
				},
				success: function(responseData) {	// on success ajax response
					// put response data in data holder
					contentHolder.html(responseData);

                    // load all js functions added to stack in fetched responseData
					$.each(zApps.winManager.getInitStack(), function(index, value) {
						// take first init function from initStack, run it and remove it from stack
						var initFunction = zApps.winManager.getInitStack().shift();
						if(!(typeof(initFunction) == 'undefined')) {
							// call returned init function with dialogWindow as context
							initFunction(self);
						}
					});

					self.hideLoader();
				},
				error: function(jqXHR, textStatus, errorThrown) {	// on error ajax response
                    var template = ''
                        + '<div id="' + options.id + '" class="modal" tabindex="-1" style="display: block; padding-right: 15px;">'
                        + '    <div class="modal-dialog"><!-- Add "modal-sm", "modal-lg", or no class for normal sized window -->'
                        + '        <div class="modal-content box">'
                        + '            <div class="modal-body" style="height:80px;">'
                        + '                Error loading content. ' + jqXHR.responseText
                        + '            </div>'
                        + '            <div class="modal-footer">'
                        + '                <button type="button" name="close" class="btn btn-default">Close</button>'
                        + '            </div>'
                        + '        </div>'
                        + '    </div>'
                        + '</div>';
                    contentHolder.html(template);

                    contentHolder.find('button[name="close"]').click(function() {
                        self.close();
                    });

					self.hideLoader();
				}
			});
		}
	};

	self.showLoader = function() {
		var loader = $('<div class="overlay loder-overlay"><i class="fa fa-refresh fa-spin"></i></div>');
		windowHolder.find('.modal-content').append(loader);
	};

	self.hideLoader = function() {
		windowHolder.find('.loder-overlay').remove();
	};

	self.highlight = function() {
		windowHolder.dialog('moveToTop');
		windowHolder.effect('highlight', {color: '#FF6140'}, 200);
		windowHolder.effect('highlight', {color: '#FF6140'}, 200);
	};

	self.onOpen = function() {
		options.onOpen(self);
	};

	self.onClose = function(params) {
		options.onClose(self, params);
	};

    self.isOpen = function() {
        // if window exists
        if(windowHolder.html().length > 0)
        {
            // and is opened
            if(windowHolder.dialog('isOpen') === true)
                return true;
        }
        return false;
    };

	self.close = function(params) {
		params = $.extend({}, params);

		// remove holder
		windowHolder.remove();

		// destroy window
		zApps.winManager.destroyWindow(options.id);

        blurElement.removeClass('blurred');

       // trigger onClose event
		self.onClose(params);
	};

	(function(params) {
		// merge params with options
		if(params === undefined || params === null || params === false)
			params = {};
		options = $.extend(options, params);

		// check if id is set
		if(options.id === null || options.id == '')
			throw "Window id is not defined!";

		// define window holder
		windowHolder = $('<div id="window_' + options.id + '"></div>');
	})(params);
};