/**
 * Win manager
 */
(function() {
    'use strict'

    // Private properties
    var windows = [];

    // Exposed object
    window.winManager = {

        createWindow: function createWindow(options) {
            var win = options;
            windows.push(win);
        },

        getWindows: function getWindows() {
            return windows;
        },
    };
})();
