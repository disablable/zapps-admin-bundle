/**
 * Dialog window
 */
(function() {
    'use strict'

    var windowHolder = null;
    var options = {
        containerSelector: null,
        zIndex: 1500,
    };
    var $windowContainer = null;
    var $closeButton = null;
    var $titleContainer = null;
    var $contentContainer = null;

    window.dialogWindow = {

        init: function init(options) {
            this.setOptions(options);
        },

        setOptions: function setOptions(opt) {
            options = $.extend({}, options, opt || {});
        },

        getOptions: function getOptions() {
            return options;
        },

        open: function open() {
            var template = ''
                + '<div class="modal" tabindex="-1" style="display: block; padding-right: 15px;">'
                + '    <div class="modal-dialog"><!-- Add "modal-sm", "modal-lg", or no class for normal sized window -->'
                + '        <div class="modal-content">'
                + '            <div class="modal-header">'
                + '                <button type="button" class="close"><span>×</span></button>'
                + '                <h4 class="modal-title">asddas</h4>'
                + '            </div>'
                + '            <div class="modal-body">'
                + '                ...'
                + '            </div>'
                // + '            <div class="modal-footer">'
                // + '                <button type="button" class="btn btn-default">Close</button>'
                // + '                <button type="button" class="btn btn-primary">Save changes</button>'
                // + '            </div>'
                + '        </div>'
                + '    </div>'
                + '</div>';

            // define window container
            $windowContainer = $(template);

            // define close button
            $closeButton = $windowContainer.find('.modal-header button.close');

            // define title container
            $titleContainer = $windowContainer.find('.modal-title');

            // define content container
            $contentContainer = $windowContainer.find('.modal-body');

            // set z-index
            $windowContainer.css('zIndex', options.zIndex + 1);

            // fix position
            $windowContainer.css('position', 'fixed');

            // attach close handler
            $closeButton.click({'context': this}, function(e) {
                e.data.context.close(); // reference to "dialogWindow" object
                console.log(e.data.context);
            }).bind(this);

            // add window to container
            $windowContainer.appendTo($(options.containerSelector));

            console.log($windowContainer);
        },

        close: function close() {
            $windowContainer.off();
            $windowContainer.detach();
            $windowContainer.remove();
        }
    };
})();
