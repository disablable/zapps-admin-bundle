/**
 * photoWidget.js
 */

zApps.photoWidget = function(params) {
    var self = this;
    var options = {
        photoHolder: null,
        parentWindow: null,
        photoManagerPath: null
    };

    self.init = function() {
        // Get value from hidden input field that holds photo filename (if loaded from db)
        var filename = options.photoHolder.find('input[type="hidden"]').val();

        // If filename exist, add this photo to widget
        if(filename) {
            self.addPhotoToWidget(filename);
        }
        // else show "Choose photo" link to widget
        else {
            self.addChosePhotoLink();
        }
    };

    self.addPhotoToWidget = function(fileName) {
        // Remove "Choose photo" link
        options.photoHolder.find('a[name="choose-photo"]').remove();

        // Put photo in photoHolder;
        options.photoHolder.find('img').remove();   // Remove previus img if exists

        var $photo = $('<img />');  // Create img element and apply properties
        $photo.attr('title', fileName);
        $photo.attr('src', fileName);

        options.photoHolder.append($photo);    // Append it to photo holder

        // Put selected photo's filename to widget's hidden input field (this is used to store image filename to db).
        options.photoHolder.find('input[type="hidden"]').val(fileName);

        self.createRemovePhotoLink();
    };

    self.addChosePhotoLink = function() {
        // create "Choose photo" link
        var $choosePhotoLink = $('<a></a>');
        $choosePhotoLink.attr('href', options.photoManagerPath);
        $choosePhotoLink.attr('name', 'choose-photo');
        $choosePhotoLink.attr('class', 'btn btn-small');
        $choosePhotoLink.html('<i class="icon-picture"></i> Choose photo');

        // Append it to photo holder
        options.photoHolder.append($choosePhotoLink);

        // Bind "Choose photo" link action
        var $choosePhotoLink = options.photoHolder.find('a[name="choose-photo"]');
        $choosePhotoLink.on('click', function(e) {
            e.preventDefault();

            // Open "Photo Manager" to select photo from
            var photoManager = zApps.winManager.createWindow( {
                type: 'dialog',
                modal: true,
                id: 'photoManager',
                title: 'Photo manager',
                sourceUrl: $choosePhotoLink.prop('href'),
                parent: options.parentWindow,
                onClose: function(photoManagerWindow) {   // photoManagerWindow je prozor "Photo manager"-a koji će biti zatvoren
                    photoManagerWindow.getWindowHolder().find('input[name="select-photo"]:checked').each(function(index, value) {
                        var $image = $(value).parent('div.photo').find('img');
                        var fileName = $image.data('file-name');
                        self.addPhotoToWidget(fileName);
                    });
                },
                width: 'auto',
                height: 'auto',
                draggable: true,
                resizable: true
            });
        });
    };

    self.createRemovePhotoLink = function() {
        // Put "Remove" photo button and bind its function
        var $removeButton = $('<a class="btn btn-danger btn-small remove-photo" type="button"><i class="icon-remove icon-white"></i></a>');
        $removeButton.hide();
        options.photoHolder.append($removeButton);

        // bind "Remove photo" link action
        $removeButton.click(function(e) {
            e.preventDefault();
            // Remove img element
            options.photoHolder.find('img').remove();
            // Remove "remove photo (X)" link
            options.photoHolder.find('a.remove-photo').remove();
            // Clear photo filename for storing into db
            options.photoHolder.find('input[type="hidden"]').val('');
            // Create "Choose photo" link
            self.addChosePhotoLink();
        });

        // Show "Remove button" on mouse over photo
        options.photoHolder.on('mouseover', function(e) {
            $removeButton.stop(true, true).fadeIn();
            return false;
        });
        options.photoHolder.on('mouseleave', function(e) {
            $removeButton.stop(true, true).fadeOut();
            return false;
        });
    };

    // constructor
    (function(params) {
        // merge params with options
        if(params === undefined || params === null || params === false)
            params = {};
        options = $.extend(options, params);

        // check if photoHolder is set
        if(options.photoHolder === null || options.photoHolder == '')
            throw "Photo holder not defined!";

        // init widget
        self.init();

    })(params);
};