/**
 * app.js
 */

var zApps = {};

zApps.vars = {};

zApps.app = function() {
    // create windows manager
    zApps.winManager = new zApps.winManager();

    // message manager
    zApps.msgManger = new zApps.msgManager();

    zApps.mainContainer = new zApps.container( {
        id: 'body'
    });

    // create containers
    // var leftContainer = new zApps.container( {
    //     id: 'left-container',
    //     widthIndex: 3
    // });

    // var middleContainer = new zApps.container( {
    //     id: 'middle-container',
    //     widthIndex: 9
    // });

    // /*var rightContainer = new zApps.container( {
    //     id: 'right-container',
    //     widthIndex: 2
    // });*/

    // // create initial windows
    // var menuWindow = zApps.winManager.createWindow( {
    //     type: 'static',
    //     id: 'menu-window',
    //     title: '',
    //     sourceUrl: 'menu/',
    //     parent: leftContainer
    // });

    // /*var secondWindow = zApps.winManager.createWindow( {
    //     type: 'static',
    //     id: 'second-window',
    //     title: 'Second static window',
    //     sourceUrl: 'menu/',
    //     parent: leftContainer
    // });*/

    // var mainWindow = zApps.winManager.createWindow( {
    //     type: 'static',
    //     id: 'main-window',
    //     parent: middleContainer,
    //     onDuplicate: 'reload'
    // });
/*
    var dw1 = zApps.winManager.createWindow( {
            type: 'dialog',
            id: 'dw1',
            title: 'test 1',
            parent: mainWindow,
            onOpen: function(self) {},
            onClose: function(self) {},
            onDuplicate: 'highlight',
            draggable: true,
            resizable: true,
            width: 400,
            height: 300
    });
*/
/*
    var dw2 = zApps.winManager.createWindow( {
            type: 'dialog',
            id: 'dw2',
            title: 'test 2',
            parent: mainWindow,
            onOpen: function(self) {},
            onClose: function(self) {}
    });
*/
    //console.log(zApps.winManager.getWindows());
    // on browser close alert user
    //$(window).bind('beforeunload', function() { return "Closing this window will terminate application!"; });
};